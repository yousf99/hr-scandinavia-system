<?php

namespace App\Jobs;

use App\Mail\RejectRequestMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendRejectRequestEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $reject_request;
    protected $adminName;

    public function __construct($reject_request, $adminName)
    {
        $this->reject_request = $reject_request;
        $this->adminName = $adminName;
    }

    public function handle()
    {
        $reject_request = $this->reject_request;

        $note = $reject_request->note;
        $adminName = $this->adminName;
        $from = $reject_request->from;
        $to = $reject_request->to;
        $type = $reject_request->Type->name;

        Mail::to($reject_request->User->email)->send(new RejectRequestMail($note, $adminName, $from, $to, $type));
    }
}
