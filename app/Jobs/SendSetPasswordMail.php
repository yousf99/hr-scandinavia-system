<?php

namespace App\Jobs;

use App\Mail\SetPasswordMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendSetPasswordMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        $user = $this->user;

        $secureToken = Str::uuid();
        $tokenExpiration = now()->addMinutes(20);
        $user->secure_token = $secureToken;
        $user->token_expires_at = $tokenExpiration;
        $user->save();

        $mailData = [
            'user_id' => $user->id,
            'secure_token' => $user->secure_token,
        ];

        Mail::to($user->email)->send(new SetPasswordMail($mailData));
    }
}
