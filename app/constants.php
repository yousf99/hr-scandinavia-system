<?php

// Constants for vacation type IDs
const VACATION_TYPE_PAID = 1;
const VACATION_TYPE_EXAM = 2;
const VACATION_TYPE_SICK = 3;
const VACATION_TYPE_UNPAID = 4;

// Constants for overtime type IDs
const OVERTIME_TYPE_HOME = 5;
const OVERTIME_TYPE_COMPANY = 6;
const OVERTIME_TYPE_HOLIDAY = 7;

// Constant array for vacation and overtime type IDs
const TYPE_IDS = [
    'vacation' => [VACATION_TYPE_PAID, VACATION_TYPE_EXAM, VACATION_TYPE_SICK, VACATION_TYPE_UNPAID], // Vacation type IDs
    'overtime' => [OVERTIME_TYPE_HOME, OVERTIME_TYPE_COMPANY, OVERTIME_TYPE_HOLIDAY],    // Overtime type IDs
];
