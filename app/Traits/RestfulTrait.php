<?php

namespace App\Traits;

use App\Http\Controllers\api\v1\ApiController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


trait RestfulTrait
{

    public function apiResponse($data = null, $code = 200, $message = null)
    {
        $arrayResponse = [
            'data' => $data,
            'status' => $code == 200 || $code == 201 || $code == 204 || $code == 205,
            'message' => $message,
            'code' => $code,
        ];
        return response($arrayResponse, $code);
    }

    protected function apiErrorResponse(\Exception $exception, $code = 500, string $message = 'Error')
    {
        $arrayResponse = [
            'error' => [
                'status' => $code,
                'message' => $message,
                'details' => $exception->getMessage(),
            ]
        ];
        // return response($arrayResponse, $code);
        return $arrayResponse;
    }


    public function apiValidation($request, $array)
    {
        $validator = Validator::make($request->all(), $array);
        if ($validator->fails()) {
            return $this->apiResponse(null, ApiController::STATUS_VALIDATION, $validator->messages());
        }
        return $validator->valid();
    }
}