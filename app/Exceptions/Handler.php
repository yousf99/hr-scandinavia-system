<?php

namespace App\Exceptions;

use App\Http\Controllers\api\v1\ApiController;
use App\Traits\RestfulTrait;
//use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use spatie\Permission\Exceptions\UnauthorizedException as ExceptionSpatieUnauthorized;

class Handler extends ExceptionHandler
{

    use RestfulTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */

    public function register()
    {
        //
    }

    public function render($request, Throwable $exception)
    {
        $response = $this->handleException($request, $exception);
        return $response;
    }

    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    public function handleException($request, Throwable $exception)
    {

        if ($exception instanceof AuthenticationException) {
            return $this->apiResponse('', ApiController::STATUS_UNAUTHORIZED, $exception->getMessage());
        }

        if ($exception instanceof AuthorizationException) {
            return $this->apiResponse('', ApiController::STATUS_FORBIDDEN, $exception->getMessage());
        }

        if ($exception instanceof ExceptionSpatieUnauthorized) {
            return $this->apiResponse('', ApiController::STATUS_UNAUTHORIZED, $exception->getMessage());
        }

        if ($exception instanceof HttpException) {
            return $this->apiResponse('', ApiController::STATUS_BAD_REQUEST, $exception->getMessage());
        }

        if ($exception instanceof HttpResponseException) {
            return $this->apiResponse('', ApiController::STATUS_FORBIDDEN, $exception->getMessage());
        }

        if ($exception instanceof ValidationException) {
            return $this->apiResponse('', ApiController::STATUS_VALIDATION, $exception->getMessage());
        }

        if (config('app.debug')) {
            return parent::render($request, $exception);
        }

        return $this->apiResponse('', ApiController::STATUS_NOT_FOUND, $exception->getMessage());
    }
}
