<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Request;

class DateHelper
{
    public static function formatDateRange(Request $request)
    {
        $request = self::formatDate($request, 'from');
        $request = self::formatDate($request, 'to');
        if ($request->to === null) {
            $request->merge(['to' => $request->from]);
        }

        $startDate = Carbon::parse($request->from);
        $endDate = Carbon::parse($request->to);

        if ($startDate != $endDate) {
            if ($startDate > $endDate) {
                return false;
            }
            $diffInDays = $startDate->diffInDays($endDate) + 1;
            $request['hours'] = $diffInDays * 8;
        }

        return $request;
    }

    public static function formatDate(Request $request, $field)
    {
        if ($request->{$field}) {
            $date = Carbon::parse($request->{$field})->format('Y-m-d');
            $request->merge([$field => $date]);
        }

        return $request;
    }
}