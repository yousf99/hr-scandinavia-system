<?php

namespace App\Filter;

use App\Enums\RequestStatus;
use Carbon\Carbon;
use Closure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

include_once app_path('constants.php');

class RequestFilter
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (!empty($query->filter)) {
            $builder = $next($request);

            if (!empty($query->filter->today_absent)) {
                $currentDate = Carbon::today()->toDateString();

                $builder = $builder->where('status', RequestStatus::final_approved)
                    ->whereIn('type_id', TYPE_IDS['vacation'])->where('from', '<=', $currentDate)
                    ->Where('to', '>=', $currentDate)->distinct('id');
            }

            if (!empty($query->filter->type_id)) {
                $builder = $builder->where('type_id', $query->filter->type_id);
            }

            if (!empty($query->filter->status)) {
                $builder = $builder->where('status', $query->filter->status);
            }

            if (!empty($query->filter->user_id)) {
                $builder = $builder->where('user_id', $query->filter->user_id);
            }

            if (!empty($query->filter->is_overtime) && !empty($query->filter->is_vacation)) {

                throw new HttpException(Response::HTTP_BAD_REQUEST, 'You cannot use is_overtime and is_vacation together');
            }

            if (!empty($query->filter->is_vacation)) {
                $builder = $builder->where('is_overtime', false);
            }

            if (!empty($query->filter->is_overtime)) {
                $builder = $builder->where('is_overtime', true);
            }

            if (!empty($query->filter->weekly)) {
                $now = Carbon::now();
                $startOfWeek = $now->copy()->startOfWeek(Carbon::SATURDAY)->format('Y-m-d H:i');
                $endOfWeek = $now->copy()->endOfWeek(Carbon::FRIDAY)->format('Y-m-d H:i');

                $builder = $builder->where(function ($query) use ($startOfWeek, $endOfWeek) {
                    $query->whereBetween('from', [$startOfWeek, $endOfWeek])
                        ->orWhereBetween('to', [$startOfWeek, $endOfWeek]);
                });
            }

            if (!empty($query->filter->today)) {
                $now = Carbon::now();
                $startOfDay = $now->startOfDay()->format('Y-m-d H:i');
                $endOfDay = $now->endOfDay()->format('Y-m-d H:i');

                $builder = $builder->where(function ($query) use ($startOfDay, $endOfDay) {
                    $query->whereBetween('from', [$startOfDay, $endOfDay])
                        ->orWhereBetween('to', [$startOfDay, $endOfDay]);
                });
            }

            return $builder;
        }
        return $next($request);
    }
}
