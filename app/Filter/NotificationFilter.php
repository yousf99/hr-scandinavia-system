<?php

namespace App\Filter;

use Illuminate\Http\Request;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class NotificationFilter
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (!empty($query->filter)) {
            $builder = $next($request);

            if (empty($query->filter->read) && empty($query->filter->unread)) {
                return $builder;
            }

            if (!empty($query->filter->read) && !empty($query->filter->unread)) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'You cannot use read and unread together');
            }

            (!empty($query->filter->read)) ? $builder->where('is_read', true) : $builder->where('is_read', false);

            return $builder;
        }
        return $next($request);
    }
}
