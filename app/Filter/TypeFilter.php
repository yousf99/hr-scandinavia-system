<?php

namespace App\Filter;

use Illuminate\Http\Request;

use Closure;
use Illuminate\Database\Eloquent\Builder;

include_once app_path('constants.php');

class TypeFilter
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (!empty($query->filter)) {
            $filters = collect($query->filter);
            $types = $next($request);

            if ($filters->has('overtime')) {
                $types = $types->whereIn('id', TYPE_IDS['overtime']);
            }

            if ($filters->has('vacation')) {
                $types = $types->whereIn('id', TYPE_IDS['vacation']);
            }
            return $types;
        }
        return $next($request);
    }
}
