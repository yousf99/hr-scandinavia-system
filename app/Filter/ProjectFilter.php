<?php

namespace App\Filter;

use Illuminate\Http\Request;

use Closure;
use Illuminate\Database\Eloquent\Builder;

class ProjectFilter
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (!empty($query->filter)) {
            $filters = collect($query->filter);
            $projects = $next($request);

            if ($filters->has('status')) {
                $projects = $projects->where('status', $filters->get('status'));
            }

            if ($filters->has('branch')) {
                $projects = $projects->where('branch_id', $filters->get('branch'));
            }

            if ($filters->has('department')) {
                $projects = $projects->where('department_id', $filters->get('department'));
            }

            if ($filters->has('user_id')) {
                $projects = $projects->whereHas('users', function (Builder $query) use ($filters) {
                    $query->where('id', $filters->get('user_id'));
                });
            }

            return $projects;
        }

        return $next($request);
    }
}