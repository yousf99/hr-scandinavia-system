<?php

namespace App\Filter\User;

use App\Enums\RequestStatus;
use App\Models\Request;
use Carbon\Carbon;
use Closure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AbsentFilter
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));
        if (!empty($query->filter)) {
            $builder = $next($request);

            if (empty($query->filter->is_present) && empty($query->filter->is_absent)) {
                return $builder;
            }

            if (!empty($query->filter->is_present) && !empty($query->filter->is_absent)) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'You cannot use is_absent and is_present together');
            }

            $currentDate = Carbon::today()->toDateString();
            $employees_absent_id = Request::whereIn('type_id', [1, 2, 3, 4])
                ->whereDate('from', '<=', $currentDate)
                ->whereDate('to', '>=', $currentDate)->pluck('user_id');


            (!empty($query->filter->is_absent)) ? $builder->whereIn('id', $employees_absent_id) : $builder->whereNotIn('id', $employees_absent_id);

            return $builder;
        }
        return $next($request);
    }
}
