<?php

namespace App\Filter\User;

use Closure;
use Illuminate\Support\Facades\DB;

class UserFilter
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        // Add search filters for full name, first name, and last name
        if (!empty($query->search)) {
            $builder = $next($request);
            $searchTerm = $query->search;

            $builder = $builder->where(function ($query) use ($searchTerm) {
                $query->where('first_name', 'like', '%' . $searchTerm . '%')
                    ->orWhere('last_name', 'like', '%' . $searchTerm . '%')
                    ->orWhere(DB::raw("CONCAT(first_name, ' ', last_name)"), 'like', '%' . $searchTerm . '%');
            });
        }

        if (!empty($query->filter)) {
            $builder = $next($request);

            if (!empty($query->filter->department_id)) {
                $builder = $builder->where('department_id', $query->filter->department_id);
            }

            if (!empty($query->filter->is_student)) {
                $builder = $builder->where('is_student', $query->filter->is_student);
            }
            return $builder;
        }

        // If no filter is applied, simply return the result of the next middleware
        return $next($request);
    }
}
