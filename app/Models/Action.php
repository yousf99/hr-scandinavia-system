<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'url', 'notification_id'];

    public function Notification()
    {
        return $this->belongsTo(Notification::class, 'notification_id');
    }
}
