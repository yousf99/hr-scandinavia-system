<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $fillable = ['start', 'end'];

    public function Users()
    {
        return $this->hasMany(User::class);
    }

    public function ScheduleHolidays()
    {
        return $this->hasMany(ScheduleHolidays::class);
    }
}
