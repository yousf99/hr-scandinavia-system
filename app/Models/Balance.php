<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function Types()
    {
        return $this->hasMany(Type::class);
    }

    public function UserBalances()
    {
        return $this->hasMany(UserBalance::class);
    }
}
