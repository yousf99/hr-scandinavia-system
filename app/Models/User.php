<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;



class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'position',
        'is_student',
        'start_date',
        'personal_email',
        'address',
        'description',
        'department_id',
        'branch_id',
        'image',
        'schedule_id',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $guard_name = 'web';

    public function guardName()
    {
        return 'web';
    }

    //Relations
    public function Requests()
    {
        return $this->hasMany(Request::class);
    }

    public function UserBalances()
    {
        return $this->hasMany(UserBalance::class);
    }

    public function Department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function Branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function Projects()
    {
        return $this->belongsToMany(Project::class, 'user_projects');
    }

    public function UserProjects()
    {
        return $this->hasMany(UserProjects::class);
    }

    public function Attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function deviceTokens()
    {
        return $this->hasMany(DeviceToken::class);
    }

    public function Schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }

    //Functions
    static function RemoveFromAmount($id, $hours, $type_balance_id)
    {
        $balance = UserBalance::where('user_id', '=', $id)->where('balance_id', '=', $type_balance_id)->first();
        if ($balance) {
            $balance->amount = $balance->amount - $hours;
            if ($balance->amount >= 0) {
                $balance->save();
                return true;
            }
        }
        return false;
    }

    static function AddToAmount($id, $hours, $type_balance_id)
    {
        $balance = UserBalance::where('user_id', '=', $id)->where('balance_id', '=', $type_balance_id)->first();
        $balance->amount = $balance->amount + $hours;
        $balance->save();
        return $hours;
    }

    public function getDuration()
    {
        $todayDate = Carbon::now();
        $startDate = Carbon::parse($this->attributes['start_date']);
        $duration = $todayDate->diffInMonths($startDate);
        return $duration;
    }

    public function AddToPaidVacation()
    {
        if ($this->getDuration() === 3) {
            return 24;
        } else if ($this->getDuration()  < 3) {
            return 0;
        } else {
            if ($this->getDuration() % 12 === 0) {
                return 24;
            } else {
                return 8;
            }
        }
    }

    public  function getFullPath()
    {
        return asset('storage/' . $this->image);
    }

    public function getFullName()
    {
        return $this->first_name . " " . $this->last_name;
    }
}
