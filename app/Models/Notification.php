<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'is_read', 'read_at', 'user_id'];

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Actions()
    {
        return $this->hasMany(Action::class);
    }
}
