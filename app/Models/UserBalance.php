<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBalance extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'balance_id', 'amount'];

    public function Balance()
    {
        return $this->belongsTo(Balance::class, 'balance_id');
    }

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
