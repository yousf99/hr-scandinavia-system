<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'factor', 'balance_id'];

    public function Requests()
    {
        return $this->hasMany(Request::class);
    }

    public function Balance()
    {
        return $this->belongsTo(Balance::class, 'balance_id');
    }
}
