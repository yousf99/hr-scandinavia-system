<?php

namespace App\Models;

use App\Enums\RequestStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    use HasFactory;

    protected $fillable = ['from', 'to', 'hours', 'status', 'description', 'type_id', 'user_id', 'project_id', 'note', 'is_overtime', 'approved_by',];

    //Relations
    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Type()
    {
        return $this->belongsTo(Type::class, 'type_id');
    }

    public function approvedBy()
    {
        return $this->belongsTo(User::class, 'approved_by');
    }

    public function Project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
