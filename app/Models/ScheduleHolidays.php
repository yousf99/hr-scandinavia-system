<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScheduleHolidays extends Model
{
    use HasFactory;

    public function Schedule()
    {
        return $this->belongsTo(Schedule::class);
    }

    public function WeekDays()
    {
        return $this->belongsTo(WeekDays::class);
    }
}
