<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    use HasFactory;

    protected $fillable = [
        'url',
        'original_name',
        'user_id',
        'project_id',
    ];

    public function Users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function Projects()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function getFilePath()
    {
        return asset('storage/' . $this->url);
    }
}
