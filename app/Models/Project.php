<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'start_date', 'end_date', 'department_id', 'branch_id', 'status', 'description'];

    //Relations
    public function Users()
    {
        return $this->belongsToMany(User::class, 'user_projects');
    }

    public function Department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }

    public function Branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function UserProjects()
    {
        return $this->hasMany(UserProjects::class);
    }

    public function Attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function Requests()
    {
        return $this->hasMany(Request::class);
    }

    //Functions
    public function removeAllUsers()
    {
        $this->Users()->detach();
    }
}
