<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\position\PositionStoreRequest;
use App\Http\Requests\position\PositionUpdateRequest;
use App\Http\Resources\PositionResource;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PositionController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Position::class);
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            // 'show' => 'show',
            'store' => 'store',
            'update' => 'update',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'store', 'update'];
    }

    public function index(Request $request)
    {
        $positions = Position::query()->paginate($request->limit ?? $this->paginate);
        return $this->apiResponse(PositionResource::collection($positions)->response()->getData(true), ApiController::STATUS_OK, 'Positions have been retrieved successfully');
    }

    public function show(Position $position)
    {
        return $this->apiResponse(new PositionResource($position), ApiController::STATUS_OK, 'Position has been retrieved successfully');
    }

    public function store(PositionStoreRequest $request)
    {
        try {
            $position = Position::create($request->all());
            return $this->apiResponse(new PositionResource($position), ApiController::STATUS_CREATED, 'Position has been created successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(Request $request, Position $position)
    {
        $positionRequest = new PositionUpdateRequest($position->id);
        $validator = Validator::make($request->all(), $positionRequest->rules());
        if ($validator->fails()) {
            return $this->apiResponse(null, ApiController::STATUS_VALIDATION, $validator->messages());
        }

        try {
            $position->update($request->all());
            return $this->apiResponse(new PositionResource($position), ApiController::STATUS_CREATED, 'Position has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }
}
