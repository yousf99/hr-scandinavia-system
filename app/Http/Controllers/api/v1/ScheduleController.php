<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\schedule\ScheduleStoreRequest;
use App\Http\Resources\ScheduleResource;
use App\Models\Schedule;
use App\Models\ScheduleHolidays;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ScheduleController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Schedule::class, 'schedule');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'show' => 'show',
            'store' => 'store',
            // 'update' => 'update',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'store'];
    }

    public function index(Request $http_request)
    {
        $schedules = Schedule::all();
        return $this->apiResponse(ScheduleResource::collection($schedules), ApiController::STATUS_OK, 'Schedules have been retrieved successfully');
    }

    public function store(ScheduleStoreRequest $request)
    {
        try {
            $start = Carbon::parse($request->start);
            $end = Carbon::parse($request->end);
            $schedule = Schedule::create(['start' => $start->format('H:i'), 'end' => $end->format('H:i')]);

            $holidayDays = $request->holiday_days;
            $scheduleHolidaysData = [];
            foreach ($holidayDays as $day) {
                $scheduleHolidaysData[] = [
                    'schedule_id' => $schedule->id,
                    'day' => $day,
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }

            ScheduleHolidays::insert($scheduleHolidaysData);

            return $this->apiResponse(new ScheduleResource($schedule), ApiController::STATUS_OK, 'New Schedule has been created successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function show(Schedule $schedule)
    {
        //
    }

    public function edit(Schedule $schedule)
    {
        //
    }

    public function update(Request $request, Schedule $schedule)
    {
        //
    }

    public function destroy(Schedule $schedule)
    {
        //
    }
}
