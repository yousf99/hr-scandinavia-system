<?php

namespace App\Http\Controllers\api\v1;

use App\Enums\RequestStatus;
use App\Filter\RequestFilter;
use App\Sort\RequestSort;
use App\Http\Requests\request\AdminAddRequest;
use App\Http\Requests\request\RejectRequest;
use App\Http\Requests\request\RequestStoreRequest;
use App\Http\Resources\RequestResource;
use App\Mail\NewRequestFinalApproveMail;
use App\Mail\NewRequestFirstApproveMail;
use App\Models\Request;
use App\Models\Schedule;
use App\Models\Type;
use App\Models\User;
use Carbon\Carbon;
use DateTimeZone;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Mail;

include_once app_path('constants.php');

class RequestController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Request::class, 'request');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'approveRequest' => 'approveRequest',
            'rejectRequest' => 'rejectRequest',
            'addRequestByAdmin' => 'addRequestByAdmin'
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'addRequestByAdmin'];
    }

    public function index(HttpRequest $http_request)
    {
        $requests = app(Pipeline::class)
            ->send(Request::query())
            ->through([
                RequestFilter::class,
                RequestSort::class,
            ])->thenReturn()->paginate($http_request->limit ?? $this->paginate);
        $requests->LoadMissing('User');
        return $this->apiResponse(RequestResource::collection($requests)->response()->getData(true), ApiController::STATUS_OK, 'Requests have been retrieved successfully');
    }

    public function show(Request $request)
    {
        if (!$request) {
            return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'Request not found');
        }
        $request->LoadMissing('User');
        return $this->apiResponse(new RequestResource($request), ApiController::STATUS_OK, 'Request has been updated successfully');
    }

    public function myRequests(HttpRequest $http_request)
    {
        $user_id = auth('sanctum')->user()->id;
        $requests = app(Pipeline::class)
            ->send(Request::query())
            ->through([
                RequestFilter::class,
                RequestSort::class,
            ])->thenReturn();
        $requests = $requests->where('user_id', $user_id)->paginate($http_request->limit ?? $this->paginate);
        return $this->apiResponse(RequestResource::collection($requests)->response()->getData(true), ApiController::STATUS_OK, 'Requests have been retrieved successfully');
    }

    public function destroy($id)
    {
        $request = Request::find($id);
        try {
            $user_id = auth('sanctum')->user()->id;
            if ($request->status === RequestStatus::pending->value) {
                $type = $request->type;
                $balance = $type->balance;

                if ($balance->name != null) {
                    $returned_balance = User::AddToAmount($request->user_id, $request->hours, $type->balance_id);
                }

                $request->delete();
                return $this->apiResponse(new RequestResource($request), ApiController::STATUS_OK, 'Deleted Successfully');
            }
            return $this->apiResponse(new RequestResource($request), ApiController::STATUS_BAD_REQUEST, 'Your Not Allowed To Delete Approved Or Rejected Request ');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function approveRequest(Request $request)
    {
        try {
            if (!$request) {
                return view('redirects/request_not_found');
            }

            $admin = auth('sanctum')->user();
            $requestToApprove = $request;

            // Check if the request can be approved
            if ($requestToApprove->status === RequestStatus::final_approved || $requestToApprove->status === RequestStatus::rejected) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'You are not allowed to approve a request with the status of "Final Approved" or "Rejected"');
            } elseif ($requestToApprove->status === RequestStatus::first_approved) {
                $requestToApprove->status = RequestStatus::final_approved;
            } else {
                $requestToApprove->status = $admin->can('final_approve_request') ? RequestStatus::final_approved : RequestStatus::first_approved;
            }

            // Set the approvedBy field with the user who approves the request
            $requestToApprove->approvedBy()->associate($admin);
            $requestToApprove->save();

            //Send Notification To Employee
            $employee = User::find($requestToApprove->user_id);
            $title = "Your request has been approved In HR System";
            $body = "Your request has been approved In HR System By " . $admin->getFullName() . ", Request Status is: " . strval($requestToApprove->stat);
            $this->sendNotification($employee, $title, $body, $requestToApprove->id, 'requests', false);

            if ($requestToApprove->status === RequestStatus::first_approved) {
                $employee = $employee->getFullName();
                $type = Type::find($requestToApprove->type_id)->name;
                $this->sendFinalApproveEmailToSuperAdmin($requestToApprove, $employee, $type);
            }

            return $this->apiResponse(new RequestResource($requestToApprove), ApiController::STATUS_OK, 'Request has been approved successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function rejectRequest(RejectRequest $reject_request_http, Request $request)
    {
        try {
            if (!$request) {
                return view('redirects/request_not_found');
            }
            $reject_request = $request;

            if ($reject_request->status === RequestStatus::final_approved->value || $reject_request->status === RequestStatus::rejected->value) {
                return $this->apiResponse(new RequestResource($reject_request), ApiController::STATUS_BAD_REQUEST, 'You are not allowed to reject a request with status final approve or rejected');
            }
            $reject_request->note = $reject_request_http->note;
            $reject_request->status = RequestStatus::rejected->value;
            $reject_request->save();

            //Send Notification To Employee
            $admin = auth('sanctum')->user();
            $employee = User::find($reject_request->user_id);
            $title = "Your request has been rejected In HR System";
            $body = "Your request has been rejeted In HR System By " . $admin->getFullName();
            $this->sendNotification($employee, $title, $body, $reject_request->id, 'requests', false);

            return $this->apiResponse(new RequestResource($reject_request->loadMissing('User')), ApiController::STATUS_OK, 'Request has been rejected successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    //Api For Email Approve Request
    public function approveRequestFromEmail(HttpRequest $http_request, $id, $admin_id)
    {
        $request = Request::find($id);
        if (!$request) {
            return view('redirects/request_not_found');
        }
        $admin = User::find($admin_id);

        // Check if the request can be approved
        if ($request->status === RequestStatus::final_approved->value || $request->status === RequestStatus::rejected->value) {
            return view('redirects/request_can_not_be_change');
        } elseif ($request->status === RequestStatus::first_approved->value) {
            if ($request->can('final_approve_request')) {
                $request->status = RequestStatus::final_approved;
                $request->approved_by = $admin_id;
                $request->save();
            } else {
                return view('redirects/final_approve_needed');
            }
        } else {
            $request->status = $admin->can('final_approve_request') ? RequestStatus::final_approved : RequestStatus::first_approved;
            $request->approved_by = $admin_id;
            $request->save();
        }

        //Send Notification To Employee
        $employee = User::find($request->user_id);
        $title = "Your request has been approved In HR System";
        $body = "Your request has been approved In HR System By " . $admin->getFullName() . ", Request Status is: " . strval($request->stat);
        $this->sendNotification($employee, $title, $body, $request->id, 'requests', false);

        if ($request->status === RequestStatus::first_approved) {
            $type = Type::find($request->type_id)->name;
            $this->sendFinalApproveEmailToSuperAdmin($request, $employee, $type);
        }

        return view('redirects/approval_page');
    }

    public function rejectRequestFromEmail($id)
    {
        $request = Request::find($id);
        if (!$request) {
            return view('redirects/request_not_found');
        }

        if ($request->status === RequestStatus::final_approved->value || $request->status === RequestStatus::rejected->value) {
            //Request is eaither `final_approve` or `rejected`
            return view('redirects/request_can_not_be_change');
        } elseif ($request->status === RequestStatus::first_approved) {
            if ($request->can('final_approve_request')) {
                $request->status = RequestStatus::rejected;
                $request->approved_by = null;
                $request->save();
            } else {
                return view('redirects/final_approve_needed');
            }
        } else {
            $request->status = RequestStatus::rejected;
            $request->approved_by = null;
            $request->save();
        }

        //Send Notification To Employee
        $employee = User::find($request->user_id);
        $title = "Your request has been rejected In HR System";
        $body = "Your request has been rejected In HR System";
        $this->sendNotification($employee, $title, $body, $request->id, 'requests', false);

        return view('redirects/rejecte_page');
    }

    public function store(RequestStoreRequest $request)
    {
        try {
            $user_id = auth('sanctum')->user()->id;
            $request->merge(['user_id' => $user_id, 'from_user' => true]);
            $adminRequest = new AdminAddRequest($request->all());
            $adminRequest->merge(['Timezone' => $request->header('Timezone')]);
            $newRequest = $this->createRequest($adminRequest);

            if ($newRequest === 2) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'Invalid time input');
            } else if ($newRequest === 3) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'The employee does not have enough credit');
            }

            if ($newRequest->status === RequestStatus::final_approved->value) {
                return $this->apiResponse(new RequestResource($newRequest), ApiController::STATUS_CREATED, 'Request has been created successfully');
            }

            return $this->sendApprovalNotification($newRequest);
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Store Field');
        }
    }

    public function addRequestByAdmin(AdminAddRequest $request)
    {
        try {
            $request->merge(['Timezone' => $request->header('Timezone')]);
            $newRequest = $this->createRequest($request);
            if ($newRequest === 2) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'Invalid time input');
            } else if ($newRequest === 3) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'The employee does not have enough credit');
            } else {
                if ($newRequest->status === RequestStatus::final_approved->value) {
                    return $this->apiResponse(new RequestResource($newRequest), ApiController::STATUS_CREATED, 'Request has been created successfully');
                }
                // return $this->sendApprovalNotification($newRequest);
            }
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    //
    private function createRequest(AdminAddRequest $request)
    {
        $employee = User::with('department')->find($request->user_id);
        $department = $employee->department;
        $user = auth('sanctum')->user();

        $status = $this->getRequestStatus($request, $employee, $department, $user);

        $type = Type::find($request->type_id);
        $isOvertime = in_array($request->type_id, TYPE_IDS['overtime']);
        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

        $timezoneString = $request->Timezone;
        $timezone = new DateTimeZone($timezoneString);
        $offsetHours = $timezone->getOffset($from) / 3600;

        $from = $from->addHours($offsetHours);
        $start_request = $from->format('Y-m-d H:i');
        $to = $to->addHours($offsetHours);
        $end_request = $to->format('Y-m-d H:i');

        if ($isOvertime) {
            $hours = $this->calculateOvertimeHours($from, $to, $employee, $type->name);
        } else {
            $hours = $this->calculateHours($from, $to, $employee);
        }

        if ($hours === null) {
            return 2;
        }

        if ($type->balance_id != null) {
            $transaction_on_balance = User::RemoveFromAmount($employee->id, $hours, $type->balance_id);
            if (!$transaction_on_balance) {
                return 3;
            }
        }

        $newRequest = Request::create([
            'from' => $start_request,
            'to' => $end_request,
            'hours' => $hours,
            'user_id' => $request->user_id,
            'description' => $request->description,
            'type_id' => $request->type_id,
            'status' => $status,
            'approved_by' => $request->approved_by,
            'project_id' => $request->project_id,
            'is_overtime' => $isOvertime,
        ]);

        return $newRequest;
    }

    private function getRequestStatus($request, $employee, $department, $user)
    {
        // if the request is not from the user this mean an admin make a request for the user
        if (!$request->from_user) {
            $request->merge(['approved_by' => $user->id]);
            return $user->can('final_approve_request') ? RequestStatus::final_approved->value : RequestStatus::first_approved->value;
        } else {
            $status = $employee->can('final_approve_request') ? RequestStatus::final_approved->value : RequestStatus::pending->value;
            $request_type = in_array($request->type_id, TYPE_IDS['overtime']) ? "overtime" : "vacation";
            $check_permission = "first_approve_{$request_type}_" . strtolower(str_replace(' ', '_', $department->name));
            if ($employee->can($check_permission)) {
                return RequestStatus::first_approved->value;
            }
            return $status;
        }
    }

    //
    private function sendApprovalNotification($new_request)
    {
        $employee = User::with('department')->find($new_request->user_id);
        $department = $employee->department;
        $employee_name = $employee->getFullName();
        $type = Type::find($new_request->type_id);
        $type_name = $type->name;
        if ($new_request->status === RequestStatus::pending->value) {

            $permission_name = $new_request->is_overtime ? "first_approve_overtime_" . strtolower(str_replace(' ', '_', $department->name)) : "first_approve_vacation_" . strtolower(str_replace(' ', '_', $department->name));

            $users_with_permission = User::permission($permission_name)->get();
            if ($users_with_permission->isEmpty()) {
                return $this->sendFinalApproveEmailToSuperAdmin($new_request, $employee, $type_name);
            } else {
                foreach ($users_with_permission as $admin) {
                    Mail::to($admin->email)->send(new NewRequestFirstApproveMail($new_request, $employee_name, $employee->position, $department->name, $type_name, $admin->id));

                    if ($new_request->is_overtime) {
                        $title = 'New Overtime Request In HR';
                        $body = $employee_name . ' has made a new overtime request in the HR system';
                    } else {
                        $title = 'New Vacation Request In HR';
                        $body = $employee_name . ' has made a new vacation request in the HR system';
                    }
                    $this->sendNotification($admin, $title, $body, $new_request->id, 'requests', true);
                }
            }
            return $this->apiResponse(new RequestResource($new_request), ApiController::STATUS_CREATED, 'Request has been created successfully');
        }

        return $this->sendFinalApproveEmailToSuperAdmin($new_request, $employee, $type_name);
    }

    // Send Email To Super Admin And Call Function To Send Notification
    private function sendFinalApproveEmailToSuperAdmin($new_request, $employee, $type)
    {
        $super_admins = User::Permission('final_approve_request')->get();

        if ($new_request->is_overtime) {
            $title = 'New Overtime Request In HR';
            $body = $employee->getFullName() . ' has made a new overtime request in the HR system';
        } else {
            $title = 'New Vacation Request In HR';
            $body = $employee->getFullName() . ' has made a new vacation request in the HR system';
        }

        $department = $employee->department;

        foreach ($super_admins as $super_admin) {
            $this->sendNotification($super_admin, $title, $body, $new_request->id, 'requests', true);
            Mail::to($super_admin->email)->send(new NewRequestFinalApproveMail($new_request, $employee->getFullName(), $employee->position, $department->name, $type, $super_admin->id));
        }

        return $this->apiResponse(new RequestResource($new_request), ApiController::STATUS_CREATED, 'Request has been created successfully');
    }

    //Calculate the amount of hours of the request Vacation
    private function calculateHours($from, $to, $user)
    {
        // Validate time input
        if ($to->lte($from)) {
            return null; // Invalid time input
        }

        $holidays = $this->getHolidayDayNameForUser($user);
        $userSchedule = Schedule::find($user->schedule_id);
        $startSchedule = Carbon::parse($userSchedule->start);
        $endSchedule = Carbon::parse($userSchedule->end);
        $totalHours = 0;

        // Calculate total hours for the given date range
        while ($from->lt($to)) {
            // Check if the current date is not a holiday
            if (!in_array($from->format('l'), $holidays)) {
                // Calculate hours for the current day within schedule hours

                $startSchedule = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->start);
                $endSchedule = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->end);
                if (!$startSchedule->lte($from)) {
                    $start = $startSchedule;
                } else {
                    $start = max($from, $startSchedule);
                }
                $end = min($to, $endSchedule);
                if ($start->lt($end)) {
                    $totalHours += $start->diffInMinutes($end) / 60;
                }
            }
            $from->addDay();
            $from = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->start);
        }
        return $totalHours;
    }

    //Calculate the amount of hours of the request Overtime
    private function calculateOvertimeHours($from, $to, $user, $type)
    {
        // Check if "to" is greater than "from" 
        if ($to->lte($from)) {
            return null; // Invalid time input
        }

        $holidays = $this->getHolidayDayNameForUser($user);

        $userSchedule = Schedule::find($user->schedule_id);
        $fromStartTime = new Carbon($from->format('y-m-d') . ' ' . $userSchedule->start);
        $fromEndTime = new Carbon($from->format('y-m-d') . ' ' . $userSchedule->end);

        $totalHours = 0;

        if ($type === 'Overtime at Company on Holiday') {
            // Calculate the hours only for the holiday days
            $currentDate = $from->copy();
            while ($currentDate->lte($to)) {
                if (in_array($currentDate->format('l'), $holidays)) {
                    $hours = $fromStartTime->diffInHours($fromEndTime);
                    $hours += abs(($fromEndTime->minute - $fromStartTime->minute) / 60);

                    $totalHours += $hours;
                }
                $currentDate->addDay();
            }
        } elseif ($type === 'Overtime at Company') {
            // Calculate the hours for non-holiday days outside the user's schedule
            $currentDate = $from->copy();
            while ($currentDate->lte($to)) {
                if (!in_array($currentDate->format('l'), $holidays) && !$this->isNonWorkingDay($currentDate, $holidays)) {
                    if ($currentDate->isSameDay($from)) {
                        $start = max($from, $fromStartTime);
                    } else {
                        $start = $fromStartTime;
                    }
                    $end = $to;

                    $hours = $start->diffInHours($end);
                    $hours += abs(($end->minute - $start->minute) / 60);

                    $totalHours += $hours;
                }
                $currentDate->addDay();
            }
        } elseif ($type === 'Overtime at Home') {
            // Calculate the hours for days outside the user's schedule or during vacation with status final_approve
            $currentDate = $from->copy();
            while ($currentDate->lte($to)) {
                if (!$this->isNonWorkingDay($currentDate, $holidays) || $this->isVacationFinalApproved($currentDate, $user)) {
                    if ($currentDate->isSameDay($from)) {
                        $start = max($from, $fromEndTime);
                    } else {
                        $start = $fromStartTime;
                    }
                    $end = $to;
                    $hours = $start->diffInHours($end);
                    $hours += abs(($end->minute - $start->minute) / 60);

                    $totalHours += $hours;
                }
                $currentDate->addDay();
            }
        }
        return $totalHours;
    }

    private function getHolidayDayNameForUser($user)
    {
        $schedule = Schedule::find($user->schedule_id);
        $holidays = $schedule->ScheduleHolidays();
        $dayNames = $holidays->pluck('day')->toArray();
        return $dayNames;
    }

    private function isNonWorkingDay($date, $holidays)
    {
        $date = $date->format('l');
        if (in_array($date, $holidays)) {
            return true;
        }
        return false;
    }

    private function isVacationFinalApproved($date, $user)
    {
        $vacation = Request::where('user_id', $user->id)
            ->where('status', RequestStatus::final_approved->value)
            ->whereDate('from', '<=', $date)
            ->whereDate('to', '>=', $date)
            ->first();

        return !is_null($vacation);
    }
}
