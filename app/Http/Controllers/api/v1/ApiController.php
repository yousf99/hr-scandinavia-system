<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\image\UploadImageRequest;
use App\Http\Requests\images\UploadImageRequest as ImagesUploadImageRequest;
use Illuminate\Http\Request;
use App\Traits\RestfulTrait;
use Illuminate\Support\Facades\Storage;
use Twilio\Rest\Client;
use App\Models\Category;
use App\Models\Notification as NotificationModel;
use App\Models\UserDevice;
use App\Models\Request as RequestModel;
use App\Http\Resources\NotificationResource;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;

use Kreait\Firebase\Messaging\Notification;

class ApiController extends Controller
{
    use RestfulTrait;

    //Default Pagination
    public $paginate = 10;

    // Informational
    public const STATUS_CONTINUE = 100;
    public const STATUS_SWITCHING_PROTOCOLS = 101;

    // Successful
    public const STATUS_OK = 200;
    public const STATUS_CREATED = 201;
    public const STATUS_ACCEPTED = 202;
    public const STATUS_NON_AUTHORITATIVE_INFORMATION = 203;
    public const STATUS_NO_CONTENT = 204;
    public const STATUS_RESET_CONTENT = 205;
    public const STATUS_PARTIAL_CONTENT = 206;

    // Redirection
    public const STATUS_MULTIPLE_CHOICES = 300;
    public const STATUS_MOVED_PERMANENTLY = 301;
    public const STATUS_FOUND = 302;
    public const STATUS_SEE_OTHER = 303;
    public const STATUS_NOT_MODIFIED = 304;
    public const STATUS_USE_PROXY = 305;
    public const STATUS_TEMPORARY_REDIRECT = 307;
    public const STATUS_PERMANENT_REDIRECT = 308;

    // Client Error
    public const STATUS_BAD_REQUEST = 400;
    public const STATUS_UNAUTHORIZED = 401;
    public const STATUS_PAYMENT_REQUIRED = 402;
    public const STATUS_FORBIDDEN = 403;
    public const STATUS_NOT_FOUND = 404;
    public const STATUS_METHOD_NOT_ALLOWED = 405;
    public const STATUS_NOT_ACCEPTABLE = 406;
    public const STATUS_PROXY_AUTHENTICATION_REQUIRED = 407;
    public const STATUS_REQUEST_TIMEOUT = 408;
    public const STATUS_CONFLICT = 409;
    public const STATUS_GONE = 410;
    public const STATUS_LENGTH_REQUIRED = 411;
    public const STATUS_PRECONDITION_FAILED = 412;
    public const STATUS_PAYLOAD_TOO_LARGE = 413;
    public const STATUS_URI_TOO_LONG = 414;
    public const STATUS_UNSUPPORTED_MEDIA_TYPE = 415;
    public const STATUS_RANGE_NOT_SATISFIABLE = 416;
    public const STATUS_EXPECTATION_FAILED = 417;
    public const STATUS_VALIDATION = 422;
    public const STATUS_UPGRADE_REQUIRED = 426;

    // Server Error
    public const STATUS_INTERNAL_SERVER_ERROR = 500;
    public const STATUS_NOT_IMPLEMENTED = 501;
    public const STATUS_BAD_GATEWAY = 502;
    public const STATUS_SERVICE_UNAVAILABLE = 503;
    public const STATUS_GATEWAY_TIMEOUT = 504;
    public const STATUS_HTTP_VERSION_NOT_SUPPORTED = 505;


    public function upload_image(UploadImageRequest $request)
    {
        $file = $request->file('image');
        $extension = $file->getClientOriginalExtension();
        // getting image extension
        $randomName = Str::random(40) . '.' . $extension;
        $imagePath = $request->file('image')->store('images', 'public');
        // return  $imagePath;
        return $this->apiResponse($imagePath, ApiController::STATUS_OK, 'image Uploaded sucssfully');
    }

    public function Delete_image($image)
    {
        if (file_exists(storage_path('app/public/') . $image) and $image != 'images/user_default.png' and $image != 'images/user_default.jpg') {
            Storage::disk('public')->delete($image);
        }
    }

    public function upload_file($file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = $file->getClientOriginalName();
        $randomName = Str::random(40);
        $filePath = $file->storeAs('files', $randomName . '.' . $extension, 'public');
        return [
            'path' => $filePath,
            'original_name' => $filename
        ];
    }

    public function delete_file($filePath)
    {
        if (Storage::disk('public')->exists($filePath)) {
            Storage::disk('public')->delete($filePath);
            return true; // File deleted successfully
        }
        return false; // File not found
    }

    // Send Notification
    public function sendNotification($receiver, $title, $body, $model_id, $model_name, $multible_action = true)
    {
        $factory = (new Factory)->withServiceAccount(__DIR__ . '/../../../service-account.json');
        $messaging = $factory->createMessaging();
        $iconPath = 'http://hrapi.scandinavia-group.com/storage/images/logo-portrait.png';
        $clickActionUrl = 'https://hr-front.scandinavia-group.com/';



        $notification = Notification::create($title, $body);
        $userDevices = $receiver->deviceTokens->pluck('token')->toArray();

        $this->createNotification($title, $body, $receiver->id, $model_id, $model_name, true);
        try {
            foreach ($userDevices as $deviceToken) {
                $message = CloudMessage::withTarget('token', $deviceToken)
                    ->withNotification($notification)
                    ->withWebPushConfig([
                        'notification' => [
                            'icon' => $iconPath,
                            'click_action' => $clickActionUrl,
                        ],
                    ]);

                $messaging->send($message);
            }
        } catch (\Exception $e) {
            return null;
        }
    }

    //Create Notification
    public function createNotification($title, $body, $user_id, $model_id, $model, $multible_action)
    {
        $notification = NotificationModel::create([
            'title' => $title,
            'body' => $body,
            'user_id' => $user_id,
        ]);

        $url = $model . '/show' . '/' . $model_id;
        $actions = [['title' => 'show', 'url' => $url]];

        if ($multible_action) {
            $approveUrl = $url . '?action=approve';
            $rejectUrl = $url . '?action=reject';

            $actions[] = ['title' => 'approve', 'url' => $approveUrl];
            $actions[] = ['title' => 'reject', 'url' => $rejectUrl];
        }

        $notification->actions()->createMany($actions);
    }
}
