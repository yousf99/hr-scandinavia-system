<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\role\StoreRoleRequest;
use App\Http\Requests\role\UpdateRoleRequest;
use App\Http\Requests\role\GrantRoleRequest;
use App\Http\Requests\role\RevokeRoleRequest;

use App\Models\User;

use Spatie\Permission\Models\Role;
// use Spatie\Permission\Models\Permission;
use App\Models\Permission;

use App\Http\Resources\RoleResource;
use App\Http\Resources\UserResource;

class RoleController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Spatie\Permission\Models\Role::class, 'role');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'show' => 'show',
            'store' => 'store',
            'update' => 'update',
            'destroy' => 'destroy',
            'grant' => 'grant',
            'revoke' => 'revoke',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'show', 'store', 'update', 'destroy', 'grant', 'revoke'];
    }

    public function index()
    {
        $roles = Role::with('Permissions')->get();
        return $this->apiResponse(($roles), ApiController::STATUS_OK, 'Roles have been retrived successfully');
    }

    public function show(Role $role)
    {
        return $this->apiResponse(new RoleResource($role->LoadMissing('Permissions')), ApiController::STATUS_OK, 'Role has been retrived successfully');
    }

    public function store(StoreRoleRequest $request)
    {
        try {
            $role = Role::create($request->validated() + ['guard_name' => 'web']);
            if ($request->has('permissions') && is_array($request->input('permissions'))) {
                $permissionIds = $request->input('permissions');
                $role->permissions()->attach($permissionIds);
            }
            return $this->apiResponse(new RoleResource($role), ApiController::STATUS_CREATED, 'Role has been created successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        $role->update($request->validated());
        $role->permissions()->detach();
        if ($request->has('permissions') && is_array($request->input('permissions'))) {
            $permissionIds = $request->input('permissions');
            $role->permissions()->attach($permissionIds);
        }
        return $this->apiResponse(new RoleResource($role), ApiController::STATUS_OK, 'Role has been updated successfully');
    }

    public function destroy(Role $role)
    {
        try {
            $role->delete();
            return $this->apiResponse(null, ApiController::STATUS_OK, 'Role has been deleted successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function grant(GrantRoleRequest $request)
    {

        $user = User::with('roles.permissions')->findOrFail($request->user_id);
        $role_name = Role::find($request->role_id);
        $user->assignRole($role_name);
        return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'Role has been assign successfully to user');
    }

    public function revoke(RevokeRoleRequest $request)
    {
        $user = User::with('roles.permissions', 'permissions')->findOrFail($request->user_id);
        $role_name = Role::find($request->role_id);
        $user->removeRole($role_name);
        return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'Role has been revoked successfully from user');
    }
}
