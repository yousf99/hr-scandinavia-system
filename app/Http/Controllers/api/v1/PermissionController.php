<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Permission;
// use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\RoleResource;


use App\Http\Requests\permission\GrantPermissionRequest;
use App\Http\Requests\permission\PermissionUpdateRequest;
use App\Http\Requests\permission\RevokePermissionRequest;

class PermissionController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Permission::class);
        // $this->authorizeResource(Spatie\Permission\Models\Permission::class, 'permission');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'show' => 'show',
            'grant' => 'grant',
            'revoke' => 'revoke',
            'update' => 'update'
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'show', 'grant', 'revoke'];
    }

    public function index()
    {
        $permissions = Permission::all();
        return $this->apiResponse(PermissionResource::collection($permissions), ApiController::STATUS_OK, 'Permissions have been retrived successfully');
    }

    public function show(Permission $permission)
    {
        return $this->apiResponse(new PermissionResource($permission->LoadMissing('Roles')), ApiController::STATUS_OK, 'Roles of the permissions have been retrived successfully');
    }

    public function grant(GrantPermissionRequest $request, Role $role)
    {
        $role->givePermissionTo($request->permissions);
        return $this->apiResponse(new RoleResource($role->load('permissions')), ApiController::STATUS_OK, 'Permission has been added to role successfully');
    }

    public function revoke(RevokePermissionRequest $request, Role $role)
    {
        $role->revokePermissionTo($request->permissions);
        return $this->apiResponse(new RoleResource($role->load('permissions')), ApiController::STATUS_OK, 'Permission has been removed from role successfully');
    }

    public function update(PermissionUpdateRequest $request, Permission $permission)
    {
        try {
            $permission->update($request->only(
                'description',
            ));
            // $permission = $permission->update($request->description);
            return $this->apiResponse(new PermissionResource($permission), ApiController::STATUS_OK, 'Permission has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }
}
