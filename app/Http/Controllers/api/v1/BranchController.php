<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\branch\BranchStoreRequest;
use App\Http\Requests\branch\BranchUpdateRequest;
use App\Models\Branch;
use App\Http\Resources\BranchResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Branch::class);
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'store' => 'store',
            'update' => 'update',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'store'];
    }

    public function index()
    {
        $branches = Branch::all();
        return $this->apiResponse(BranchResource::collection($branches), ApiController::STATUS_OK, 'Branches have been retrieved successfully');
    }

    public function store(BranchStoreRequest $request)
    {
        try {
            $branch = Branch::create($request->all());
            return $this->apiResponse(new BranchResource($branch), ApiController::STATUS_CREATED, 'Branch has been created successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(Request $request, Branch $branch)
    {
        $branchRequest = new BranchUpdateRequest($branch->id);
        $validator = Validator::make($request->all(), $branchRequest->rules());
        if ($validator->fails()) {
            return $this->apiResponse(null, ApiController::STATUS_VALIDATION, $validator->messages());
        }

        try {
            $branch->update($request->all());
            return $this->apiResponse(new BranchResource($branch), ApiController::STATUS_CREATED, 'Branch has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }
}
