<?php

namespace App\Http\Controllers\api\v1;

use App\Filter\User\AbsentFilter;
use App\Filter\User\UserFilter;
use App\Helpers\DateHelper;
use App\Http\Requests\user\UserStoreRequest;
use App\Http\Requests\user\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Jobs\SendSetPasswordMail;
use App\Models\Attachment;
use App\Models\User;
use App\Models\UserBalance;
use App\Observers\UserObserver;
use App\Sort\UserSort;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'show' => 'show',
            'store' => 'store',
            'update' => 'update',
            'softDelete' => 'softDelete',
            'restore' => 'restore',
            'makeStudent' => 'makeStudent',
            'makeNotStudent' => 'makeNotStudent',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'store', 'restore'];
    }

    public function index(Request $http_request)
    {
        // $user_id = auth('sanctum')->user()->id;
        $users = app(Pipeline::class)->send(User::query())->through([
            UserFilter::class,
            AbsentFilter::class,
            UserSort::class,
        ])->thenReturn();
        $users = $users->paginate($http_request->limit ?? $this->paginate);
        // $users = $users->where('id', '!=', $user_id)->paginate($http_request->limit ?? $this->paginate);
        return $this->apiResponse(UserResource::collection($users)->response()->getData(true), ApiController::STATUS_OK, 'Users have been retrieved successfully');
    }

    public function show(User $user)
    {
        if (!$user) {
            return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'User not found');
        }
        $user->LoadMissing(['Attachments', 'roles']);
        return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'User has been retrived successfully');
    }

    public function store(UserStoreRequest $request)
    {
        $request->merge(['start_date' => Carbon::parse($request->start_date)->format('Y-m-d')]);
        return DB::transaction(function () use ($request) {
            $requestData = $request->all();
            if (!$request->has('image')) {
                $request->merge(['image' => 'images/User-avatar.png']);
            }

            // $user = User::create(array_merge($requestData, ['image' => $imagePath]));
            $user = User::create(array_merge($request->all(), ['schedule_id' => 1]));

            if ($request->has('attachments') && is_array($request->input('attachments'))) {
                $attachmentIds = $request->input('attachments');
                foreach ($attachmentIds as $attachmentId) {
                    $attachment = Attachment::find($attachmentId);
                    $attachment->user_id = $user->id;
                    $attachment->save();
                }
            }

            // $userObserver = new UserObserver();
            // $userObserver->created($user);
            // SendSetPasswordMail::dispatch($user);
            $user->LoadMissing('Attachments');
            // dd($user);

            return $this->apiResponse(new UserResource($user), ApiController::STATUS_CREATED, 'User has been created successfully');
        });
    }

    public function update(Request $request, User $user)
    {
        $userRequest = new UserUpdateRequest($user->id);
        $validator = Validator::make($request->all(), $userRequest->rules());
        if ($validator->fails()) {
            return $this->apiResponse(null, ApiController::STATUS_VALIDATION, $validator->messages());
        }

        try {
            $image_path = $user->image;
            if ($request->has('image')) {
                if ($user->image !== "images/User-avatar.png") {
                    $this->delete_image($user->image);
                }
                // $image_path = $this->upload_image($request);
                $image_path = $request->image;
            }
            $request->start_date = DateHelper::formatDate($request, 'start_date');
            $user->update($request->only(
                'first_name',
                'last_name',
                'position',
                'start_date',
                'address',
                'email',
                'phone',
                'personal_email',
                'department_id',
                'branch_id',
                'description',
                'schedule_id',
                'image',
            ));
            //note fix the delete before update the user
            // $user->Attachments()->delete();
            $user->save();

            if ($request->has('attachments') && is_array($request->input('attachments'))) {
                $attachmentIds = $request->input('attachments');
                foreach ($attachmentIds as $attachmentId) {
                    $attachment = Attachment::find($attachmentId);
                    $attachment->user_id = $user->id;
                    $attachment->save();
                }
            }
            // $user->refresh();
            $user->LoadMissing('Attachments');

            return $this->apiResponse(new UserResource($user), ApiController::STATUS_CREATED, 'User has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function softDelete(User $user)
    {
        $user->delete();
        return $this->apiResponse([], ApiController::STATUS_OK, 'user deleted successfully');
    }

    public function restore($id)
    {
        $user = User::onlyTrashed()->find($id);
        if ($user) {
            $user->restore();
            return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'User restored successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'This object does not exist');
    }

    public function makeStudent(User $user)
    {
        try {
            if ($user->is_student) {
                return $this->apiResponse(new UserResource($user), ApiController::STATUS_CREATED, 'This user is already a student');
            }
            $user->is_student = true;
            $user->save();
            $user_balance = UserBalance::create(['user_id' => $user->id, 'balance_id' => 2, 'amount' => 48]);
            return $this->apiResponse(new UserResource($user), ApiController::STATUS_CREATED, 'The process done successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function makeNotStudent(User $user)
    {
        try {
            if (!$user->is_student) {
                return $this->apiResponse(new UserResource($user), ApiController::STATUS_CREATED, 'This user is already a normal employee');
            }
            $user->is_student = false;
            $user->save();
            $user_balance = UserBalance::where('user_id', '=', $user->id)->where('balance_id', '=', 2)->delete();
            return $this->apiResponse(new UserResource($user), ApiController::STATUS_CREATED, 'The process done successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }
}
