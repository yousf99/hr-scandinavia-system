<?php

namespace App\Http\Controllers\api\v1;

use App\Filter\TypeFilter;
use App\Http\Requests\type\TypeStoreRequest;
use App\Http\Requests\type\TypeUpdateRequest;
use App\Http\Resources\TypeResource;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pipeline\Pipeline;

class TypeController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Type::class, 'type');
    }

    protected function resourceAbilityMap()
    {
        return [
            // 'index' => 'index',
            'show' => 'show',
            'store' => 'store',
            'update' => 'update',
            'softDelete' => 'softDelete',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        // return ['index', 'store'];
        return ['store'];
    }

    public function index(Request $request)
    {
        $types = app(Pipeline::class)->send(Type::query())->through([
            TypeFilter::class
        ])->thenReturn()->paginate($request->limit ?? $this->paginate);
        $types->LoadMissing('Balance');
        return $this->apiResponse(TypeResource::collection($types)->response()->getData(true), ApiController::STATUS_OK, 'Types have been retrieved successfully');
    }

    public function show(Type $type)
    {
        try {
            return $this->apiResponse(new TypeResource($type->LoadMissing('Balance')), ApiController::STATUS_OK, 'Type has been retrieved successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function store(TypeStoreRequest $request)
    {
        $request->validated();
        try {
            $type = Type::create($request->all());
            return $this->apiResponse(new TypeResource($type->LoadMissing('Balance')), ApiController::STATUS_CREATED, 'Type has been created successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(Request $request, Type $type)
    {
        try {
            $typeUpdateRequest = new TypeUpdateRequest();
            $data = $request->all();

            $validation = $this->apiValidation($request, $typeUpdateRequest->rules($type->id));
            if ($validation instanceof Response) {
                return $validation;
            }

            $type->fill($data);
            $type->save();
            return $this->apiResponse(new TypeResource($type->LoadMissing('Balance')), ApiController::STATUS_CREATED, 'Type has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function softDelete(Type $type)
    {
        $type->delete();
        return $this->apiResponse([], ApiController::STATUS_OK, 'Type deleted successfully');
    }

    public function myTypes(Request $request)
    {

        $types = app(Pipeline::class)->send(Type::query())->through([
            TypeFilter::class
        ])->thenReturn();
        // $types->LoadMissing('Balance');
        $user = auth('sanctum')->user();
        if ($user->is_student == 0) {
            $types = $types->where('id', '!=', 2);
        }
        $types = $types->paginate($request->limit ?? $this->paginate);
        return $this->apiResponse(TypeResource::collection($types)->response()->getData(true), ApiController::STATUS_OK, 'Types have been retrieved successfully');
    }
}
