<?php

namespace App\Http\Controllers\api\v1;

use App\Enums\ProjectStatus;
use App\Enums\RequestStatus;
use App\Models\Department;
use App\Models\Project;
use App\Models\Request;
use App\Models\User;
use App\Models\UserBalance;
use Carbon\Carbon;

include_once app_path('constants.php');

class StatisticsController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Statistics::class, StatisticsPolicy::class);
    }

    protected function resourceAbilityMap()
    {
        return [
            'attendanceStatistics' => 'attendanceStatistics',
            'projectsStatistics' => 'projectsStatistics',
            'userBalanceStatistics' => 'userBalanceStatistics',
            'userOvertimeStatistics' =>     'userOvertimeStatistics'
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['attendanceStatistics', 'projectsStatistics', 'userBalanceStatistics', 'userOvertimeStatistics'];
    }

    public function attendanceStatistics()
    {
        $employees_total = User::count();
        $employees_absent_ids = $this->getAbsentEmployeesIds();
        $employees_absent = $employees_absent_ids->count();
        $employees_present = $employees_total - $employees_absent;

        $department_stats = [];
        $departments = Department::all(); // Assuming you have a Department model and table

        foreach ($departments as $department) {
            $department_id = $department->id;
            $department_name = strtolower(str_replace(' ', '_', $department->name));
            $total_employees = $this->getDepartmentTotalEmployees($department_id);
            $absent_employees = $this->getAbsentEmployeesCountByDepartment($department_id, $employees_absent_ids);
            $present_employees = $total_employees - $absent_employees;

            $department_stats[$department_name] = [
                'total' => $total_employees,
                'present' => $present_employees,
            ];
        }

        $attendance_array = [
            'employees_total' => $employees_total,
            'employees_present' => $employees_present,
            'employees_absent' => $employees_absent,
            'department_stats' => $department_stats,
        ];

        return $this->apiResponse($attendance_array, ApiController::STATUS_OK, 'Attendance Statistics have been retrieved successfully');
    }


    public function projectsStatistics()
    {
        $projects = Project::all()->count();
        $working_projects = Project::where('status', ProjectStatus::in_progress)->count();
        $pending_projects = Project::where('status', ProjectStatus::pending)->count();
        // $done_projects = Project::where('status', ProjectStatus::done)->count();

        $projects_array = [
            'projects_count' => $projects,
            'working_projects' => $working_projects,
            'pending_projects' => $pending_projects
        ];

        return $this->apiResponse($projects_array, ApiController::STATUS_OK, 'Projects Statistics has been restored successfully');
    }

    public function userBalanceStatistics($user_id)
    {
        $rest_paid_balance = $this->getBalance($user_id, 1);
        $taken_paid_balance = $this->getTakenBalance($user_id, VACATION_TYPE_PAID);
        $total_paid_balance = $rest_paid_balance + $taken_paid_balance;

        $rest_exam_balance = $this->getBalance($user_id, 2);
        $taken_exam_balance = $this->getTakenBalance($user_id, VACATION_TYPE_EXAM);
        $total_exam_balance = $rest_exam_balance + $taken_exam_balance;

        $vacations_array = [
            'total_paid_vacations' => $total_paid_balance,
            'total_exam_vacations' => $total_exam_balance,
            'rest_paid_balance' => $rest_paid_balance,
            'rest_exam_balance' => $rest_exam_balance,
            'taken_paid_balance' => $taken_paid_balance,
            'taken_exam_balance' => $taken_exam_balance,
        ];

        return $this->apiResponse($vacations_array, ApiController::STATUS_OK, 'User vacations statistics have been retrieved successfully');
    }

    public function userOvertimeStatistics($user_id)
    {
        $overtime_on_disk_on_holiday = Request::where('status', RequestStatus::final_approved)->where('user_id', $user_id)
            ->where('type_id', OVERTIME_TYPE_HOLIDAY)->sum('hours');
        $overtime_on_disk = Request::where('status', RequestStatus::final_approved)->where('user_id', $user_id)
            ->where('type_id', OVERTIME_TYPE_COMPANY)->sum('hours');
        $overtime_on_home = Request::where('status', RequestStatus::final_approved)->where('user_id', $user_id)
            ->where('type_id', OVERTIME_TYPE_HOME)->sum('hours');

        $overtime_array = [
            'overtime_on_disk_on_holiday' => $overtime_on_disk_on_holiday,
            'overtime_on_disk' => $overtime_on_disk,
            'overtime_on_home' => $overtime_on_home,
        ];

        return $this->apiResponse($overtime_array, ApiController::STATUS_OK, 'User Overtime statistics have been retrieved successfully');
    }

    public function myOvertimeStatistics()
    {
        $user_id = auth('sanctum')->user()->id;
        return $this->userOvertimeStatistics($user_id);
    }

    public function myBalanceStatistics()
    {
        $user_id = auth('sanctum')->user()->id;
        return $this->userBalanceStatistics($user_id);
    }

    //Functions to simplify the code Attendance Statistics
    private function getAbsentEmployeesIds()
    {
        $currentDate = Carbon::today()->toDateString();

        return Request::where('status', RequestStatus::final_approved)
            ->whereIn('type_id', TYPE_IDS['vacation'])
            ->where(function ($query) use ($currentDate) {
                $query->where('from', '<=', $currentDate)
                    ->where('to', '>=', $currentDate);
            })->distinct('user_id')->pluck('user_id');
    }

    private function getAbsentEmployeesCountByDepartment($departmentId, $absentEmployeesIds)
    {
        $departmentEmployeesIds = User::where('department_id', $departmentId)->pluck('id');

        return User::whereIn('id', $departmentEmployeesIds)
            ->whereIn('id', $absentEmployeesIds)
            ->count();
    }

    private function getDepartmentTotalEmployees($departmentId)
    {
        return User::where('department_id', $departmentId)->count();
    }

    ////Functions to simplify the code User Balance Statistics
    private function getBalance($user_id, $balance_id)
    {
        return UserBalance::where('user_id', $user_id)
            ->where('balance_id', $balance_id)
            ->pluck('amount')
            ->first();
    }

    private function getTakenBalance($user_id, $type_id)
    {
        return Request::where('user_id', $user_id)
            ->where('status', RequestStatus::final_approved->value)
            ->where('type_id', $type_id)
            ->sum('hours');
    }
}
