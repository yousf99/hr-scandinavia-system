<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\auth\ChangePasswordRequest;
use App\Http\Requests\auth\DeviceTokenRequest;
use App\Http\Requests\auth\ForgetPasswordRequest;
use App\Http\Requests\auth\LoginRequest;
use App\Http\Requests\auth\SetPasswordRequest;
use App\Http\Requests\auth\UpdateProfileRequest;
use App\Http\Resources\AuthResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Observers\UserObserver;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{
    public function login(LoginRequest $request)
    {
        $request->validated();
        $user = User::where('email', $request->email)->first();
        if (!$user || !Hash::check($request->password, $user->password)) {
            return $this->apiResponse(null, ApiController::STATUS_VALIDATION, 'These credentials do not match our records.');
        }
        $token = $user->createToken(config('app.name'))->plainTextToken;
        return $this->apiResponse(['user' => new AuthResource($user), 'token' => $token], ApiController::STATUS_OK, 'You have successfully logged in');
    }

    public function logout()
    {
        $user = auth()->user();
        $token = $user->currentAccessToken();
        $auth_token = $user->currentAccessToken()->token;
        if ($token->delete()) {
            $user->deviceTokens()->where('auth_token', $auth_token)->delete();
            return $this->apiResponse(null, self::STATUS_OK, 'Signed out successfully');
        }

        return $this->apiResponse(null, self::STATUS_BAD_REQUEST, 'There is something wrong with deleting the current token');
    }

    public function deviceToken(DeviceTokenRequest $request)
    {
        $user = auth('sanctum')->user();
        $token = $user->currentAccessToken()->token;
        $deviceToken = $request->device_token;

        // Check if the device token already exists
        $existingDeviceToken = $user->deviceTokens()->where('token', $deviceToken)->first();

        if (!$existingDeviceToken) {
            // If the device token does not exist, create a new entry
            $user->deviceTokens()->create(['token' => $deviceToken, 'auth_token' => $token]);
        }

        return $this->apiResponse([], ApiController::STATUS_OK, 'The Token for this device is stored successfully');
    }

    public function me()
    {
        $user_id = auth('sanctum')->user()->id;
        $user = User::find($user_id)->load('roles.Permissions');
        return $this->apiResponse(new AuthResource($user), ApiController::STATUS_OK, 'Profile has been retrieved successfully');
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user_id = auth('sanctum')->user()->id;
        $user = User::find($user_id);
        $image_path = $user->image;
        if ($user->image !== $request->image && $user->image !== "images/User-avatar.png") {
            $this->delete_image($user->image);
            $image_path = $request->image;
        } else if ($user->image !== $request->image && $user->image === "images/User-avatar.png") {
            $image_path = $request->image;
        }
        $user->update($request->only(
            'first_name',
            'last_name',
            'address',
            'phone',
            'personal_email',
        ));
        $user->fill(['image' => $image_path]);
        $user->save();

        return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'Profile has been updated successfully');
    }

    public function forgetPassword(ForgetPasswordRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return $this->apiResponse([], ApiController::STATUS_NOT_FOUND, 'There is no account for this email');
        }

        // SendResetPasswordMail::dispatch($user);
        $userObserver = new UserObserver();
        $userObserver->forgetPassword($user);

        return $this->apiResponse([], ApiController::STATUS_OK, 'Password reset link sent successfully');
    }

    public function setPassword(SetPasswordRequest $request)
    {
        $token = $request->input('token');
        $userId = $request->input('user_id');

        // Retrieve the user record by user ID and security token
        $user = User::where('id', $userId)
            ->where('secure_token', $token)
            ->where('token_expires_at', '>=', Carbon::now())
            ->first();

        if (!$user) {
            // Invalid token or expired token
            return $this->apiResponse([], ApiController::STATUS_BAD_REQUEST, 'Invalid or expired token');
        }

        // Update the user's password
        $user->password = Hash::make($request->input('password'));
        $user->secure_token = null;
        $user->token_expires_at = null;
        $user->save();

        // Delete all device tokens for the user
        $user->deviceTokens()->delete();
        // $user()->token()->delete();


        return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'Password Set Successfully');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth('sanctum')->user();
        $user->password = Hash::make($request->password);
        $user->save();

        return $this->apiResponse(new UserResource($user), ApiController::STATUS_OK, 'Password Reset Successfully');
    }
}
