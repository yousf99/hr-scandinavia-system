<?php

namespace App\Http\Controllers\api\v1;

use App\Filter\NotificationFilter;
use App\Http\Resources\NotificationResource;
use App\Models\Notification;
use App\Sort\NotificationSort;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;

class NotificationController extends ApiController
{
    public function index(Request $http_request)
    {
        $notifications = app(Pipeline::class)->send(Notification::query())->through([
            NotificationFilter::class,
            NotificationSort::class,
        ])->thenReturn();
        $notifications = $notifications->paginate($http_request->limit ?? $this->paginate);
        return $this->apiResponse(NotificationResource::collection($notifications)->response()->getData(true), ApiController::STATUS_OK, 'Notifications have been retrieved successfully');
    }


    public function myNotifications(Request $http_request)
    {
        $user_id = auth('sanctum')->user()->id;

        $notifications = app(Pipeline::class)->send(Notification::query())
            ->through([
                NotificationFilter::class,
                NotificationSort::class,
            ])->thenReturn();
        $notifications = $notifications->where('user_id', $user_id)->paginate($http_request->limit ?? $this->paginate);
        return $this->apiResponse(NotificationResource::collection($notifications)->response()->getData(true), ApiController::STATUS_OK, 'Notifications have been retrieved successfully');
    }
}
