<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\department\DepartmentStoreRequest;
use App\Http\Requests\department\DepartmentUpdateRequest;
use App\Http\Resources\DepartmentResource;
use App\Models\Department;
use App\Sort\DepartmentSort;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Pipeline;
use Spatie\Permission\Models\Permission;

class DepartmentController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Department::class, 'department');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'store' => 'store',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'store'];
    }

    public function index(Request $request)
    {
        $departments = Department::all();
        return $this->apiResponse(DepartmentResource::collection($departments->append('number_of_users')), ApiController::STATUS_OK, 'Departments have been retrieved successfully');

        $departments = app(Pipeline::class)
            ->send(Department::query())
            ->through([
                DepartmentSort::class,
            ])->thenReturn();
        $departments = $departments->paginate($request->limit ?? $this->paginate);
        return $this->apiResponse(DepartmentResource::collection($departments)->response()->getData(true), ApiController::STATUS_OK, 'Departments have been retrieved successfully');
    }

    public function store(DepartmentStoreRequest $request)
    {
        try {
            $department = Department::create($request->all());
            $permissionName = strtolower(str_replace(' ', '_', $department->name));
            Permission::create(['name' => "first_approve_overtime_$permissionName", 'guard_name' => "web"]);
            Permission::create(['name' => "first_approve_vacation_$permissionName", 'guard_name' => "web"]);
            return $this->apiResponse(new DepartmentResource($department), ApiController::STATUS_CREATED, 'Department has been created successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(DepartmentUpdateRequest $request, Department $department)
    {
        try {
            $oldDepartmentName = strtolower(str_replace(' ', '_', $department->name));
            $department->update($request->all());

            $newDepartmentName = strtolower(str_replace(' ', '_', $request->name));

            // Update the first_approve_overtime {department name}
            $overtimePermission = Permission::where('name', "first_approve_overtime_$oldDepartmentName")->first();
            $overtimePermission->name = "first_approve_overtime_$newDepartmentName";
            $overtimePermission->save();

            // Update the first_approve_vacation {department name}
            $vacationPermission = Permission::where('name', "first_approve_vacation_$oldDepartmentName")->first();
            $vacationPermission->name = "first_approve_vacation_$newDepartmentName";
            $vacationPermission->save();

            return $this->apiResponse(new DepartmentResource($department), ApiController::STATUS_OK, 'Department has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }


    public function destroy(Department $department)
    {
        try {
            $department->delete();
            $departmentName = strtolower(str_replace(' ', '_', $department->name));
            Permission::where('name', "LIKE", "%$departmentName%")->delete();
            return $this->apiResponse(null, ApiController::STATUS_OK, 'Department has been deleted successfully');
        } catch (\Exception $e) {
            return $this->apiResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }
}
