<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Helpers\DateHelper;
use App\Filter\ProjectFilter;
use App\Http\Resources\ProjectResource;
use Illuminate\Pipeline\Pipeline;
use App\Http\Requests\project\ProjectStoreRequest;
use App\Http\Requests\project\ProjectUpdateRequest;
use App\Http\Requests\project\ProjectUserRequest;
use App\Models\Attachment;
use App\Sort\ProjectSort;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ProjectController extends ApiController
{
    public function __construct()
    {
        $this->authorizeResource(Project::class, 'project');
    }

    protected function resourceAbilityMap()
    {
        return [
            'index' => 'index',
            'show' => 'show',
            'store' => 'store',
            'update' => 'update',
            'destroy' => 'destroy',
            'add_user_to_project' => 'add_user_to_project',
            'remove_user_from_project' => 'remove_user_from_project',
        ];
    }

    protected function resourceMethodsWithoutModels()
    {
        return ['index', 'store', 'my_projects'];
    }

    public function index(Request $request)
    {
        $projects = app(Pipeline::class)->send(Project::query())->through([
            ProjectFilter::class,
            ProjectSort::class,
        ])->thenReturn()->paginate($request->limit ?? $this->paginate);
        $projects->LoadMissing('Users');
        return $this->apiResponse(ProjectResource::collection($projects)->response()->getData(true), ApiController::STATUS_OK, 'Projects have been retrieved successfully');
    }

    public function show(Project $project)
    {
        return $this->apiResponse(new ProjectResource($project->LoadMissing(['Users', 'Attachments'])), ApiController::STATUS_OK, 'Project has been retrieved successfully');
    }

    public function store(ProjectStoreRequest $request)
    {
        try {
            $request = DateHelper::formatDate($request, 'start_date');
            $request = DateHelper::formatDate($request, 'end_date');

            if (!$request->start_date && $request->end_date) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'Can not insert end time without start time');
            }

            $start_date = $request->start_date;
            $end_date = $request->end_date;

            if ($start_date && $end_date && $start_date > $end_date) {
                return $this->apiResponse(null, ApiController::STATUS_BAD_REQUEST, 'Check From Time Format');
            }

            $project = Project::create($request->all());

            //Add Employee to Project
            if ($request->has('user_id') && is_array($request->input('user_id'))) {
                $userIds = $request->input('user_id');
                foreach ($userIds as $userId) {
                    $project->UserProjects()->create(['user_id' => $userId]);
                }
            }

            // Add Attachments to Project
            if ($request->has('attachments') && is_array($request->input('attachments'))) {
                $attachmentIds = $request->input('attachments');
                foreach ($attachmentIds as $attachmentId) {
                    $attachment = Attachment::find($attachmentId);
                    $attachment->project_id = $project->id;
                    $attachment->save();
                }
            }

            return $this->apiResponse(new ProjectResource($project), ApiController::STATUS_CREATED, 'Project has been created successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(ProjectUpdateRequest $request, Project $project)
    {
        try {

            $request = DateHelper::formatDate($request, 'start_date');
            $request = DateHelper::formatDate($request, 'end_date');
            $project->removeAllUsers();
            if ($request->has('user_id') && is_array($request->input('user_id'))) {
                $userIds = $request->input('user_id');
                foreach ($userIds as $userId) {
                    $project->UserProjects()->create(['user_id' => $userId]);
                }
            }
            $project->update($request->all());
            $project->Attachments()->delete();
            $project->save();

            if ($request->has('attachments') && is_array($request->input('attachments'))) {
                $attachmentIds = $request->input('attachments');
                foreach ($attachmentIds as $attachmentId) {
                    $attachment = Attachment::find($attachmentId);
                    $attachment->project_id = $project->id;
                    $attachment->save();
                }
            }
            $project->refresh();
            return $this->apiResponse(new ProjectResource($project->LoadMissing(['Users', 'Attachments'])), ApiController::STATUS_OK, 'Project has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function destroy(Project $project)
    {
        $project->delete();
        return $this->apiResponse([], ApiController::STATUS_OK, 'Projects deleted Successfully');
    }

    public function addUserToProject(ProjectUserRequest $request, Project $project)
    {
        try {
            $users_array = $project->UserProjects->pluck('user_id')->toArray();
            if (in_array($request->user_id, $users_array)) {
                return $this->apiResponse(null, ApiController::STATUS_OK, 'User already in this project');
            }
            $project->UserProjects()->create(['user_id' => $request->user_id]);
            return $this->apiResponse(new ProjectResource($project), ApiController::STATUS_OK, 'The User Successfully added to the project');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function removeUserFromProject(ProjectUserRequest $request, Project $project)
    {
        try {
            $users_array = $project->UserProjects->pluck('user_id')->toArray();
            if (!in_array($request->user_id, $users_array)) {
                return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'User not found in this project');
            }
            $project->Users()->detach($request->user_id);
            return $this->apiResponse(null, ApiController::STATUS_OK, 'User has been removed from the project successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function myProjects(Request $request)
    {
        $user_id = auth('sanctum')->user()->id;

        $projects = app(Pipeline::class)->send(
            Project::whereHas('users', function (Builder $query) use ($user_id) {
                $query->where('users.id', $user_id);
            })
        )->through([
            ProjectFilter::class,
            ProjectSort::class,
        ])->thenReturn();

        $projects = $projects->paginate($request->limit ?? $this->paginate);
        return $this->apiResponse(ProjectResource::collection($projects)->response()->getData(true), ApiController::STATUS_OK, 'Projects have been retrieved successfully');
    }
}
