<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Requests\balance\BalanceStoreRequest;
use App\Http\Requests\balance\BalanceUpdateRequest;
use App\Http\Requests\balance\RemoveUserBalanceRequest;
use App\Http\Requests\balance\UserBalanceRequest;
use App\Http\Resources\BalanceResource;
use App\Models\Balance;
use App\Models\User;
use App\Models\UserBalance;

class BalanceController extends ApiController
{
    // public function __construct()
    // {
    //     $this->authorizeResource(Balance::class, 'balance');
    // }

    // protected function resourceAbilityMap()
    // {
    //     return [
    //         'index' => 'index',
    //         'show' => 'show',
    //         'store' => 'store',
    //         'update' => 'update',
    //         'addBalanceToUser' => 'addBalanceToUser',
    //         'removeBalanceFromUser' => 'removeBalanceFromUser',
    //         'updateUserBalance' => 'updateUserBalance',

    //     ];
    // }

    // protected function resourceMethodsWithoutModels()
    // {
    //     return ['index', 'store', 'addBalanceToUser', 'removeBalanceFromUser', 'updateUserBalance'];
    // }

    public function index()
    {
        $balances = Balance::all();
        return $this->apiResponse(BalanceResource::collection($balances), ApiController::STATUS_OK, 'Balances have been retrieved successfully');
    }

    public function show(Balance $balance)
    {
        try {
            return $this->apiResponse(new BalanceResource($balance), ApiController::STATUS_OK, 'Balance has been retrieved successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function store(BalanceStoreRequest $request)
    {
        try {
            $balance = Balance::create($request->all());
            return $this->apiResponse(new BalanceResource($balance), ApiController::STATUS_CREATED, 'Balance has been created successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function update(BalanceUpdateRequest $request, Balance $balance)
    {
        try {
            $balance->update($request->only('name',));
            return $this->apiResponse(new BalanceResource($balance), ApiController::STATUS_CREATED, 'Balance has been updated successfully');
        } catch (\Exception $e) {
            return $this->apiErrorResponse($e, ApiController::STATUS_INTERNAL_SERVER_ERROR, 'Something went wrong.');
        }
    }

    public function addBalanceToUser(UserBalanceRequest $request)
    {
        $user_balance = UserBalance::where('user_id', $request->user_id)->where('balance_id', $request->balance_id)->first();
        if (empty($user_balance)) {
            UserBalance::create($request->all());
            return $this->apiResponse(null, ApiController::STATUS_CREATED, 'Balance added to user successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_FORBIDDEN, 'The user already has this balance');
    }

    public function removeBalanceFromUser(RemoveUserBalanceRequest $request)
    {
        $user_balance = UserBalance::where('user_id', $request->user_id)->where('balance_id', $request->balance_id)->pluck('id')->toarray();
        // return $user_balance;
        if (!empty($user_balance)) {
            UserBalance::whereIn('id', $user_balance)->delete();
            return $this->apiResponse(null, ApiController::STATUS_OK, 'Balance deleted from user successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_FORBIDDEN, 'The user does not has this balance');
    }

    public function updateUserBalance(UserBalanceRequest $request, $user_id)
    {
        $user = User::find($user_id);
        if ($user) {
            $user_balance = UserBalance::where('user_id', $user_id)->where('balance_id', $request->balance_id)->first();
            if (empty($user_balance)) {
                return $this->apiResponse(null, ApiController::STATUS_FORBIDDEN, 'The user does not have this balance');
            }
            $user_balance = UserBalance::find($user_balance->id);
            // $data = $request->all();
            $user_balance->amount = $request->amount;
            $user_balance->save();
            return $this->apiResponse(null, ApiController::STATUS_CREATED, 'Balance of user updated successfully');
        }
        return $this->apiResponse(null, ApiController::STATUS_NOT_FOUND, 'User not found');
    }
}
