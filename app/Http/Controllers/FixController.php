<?php

namespace App\Http\Controllers;

use App\Http\Controllers\api\v1\ApiController;
use App\Mail\NewRequestFinalApproveMail;
use App\Models\Notification;
use App\Models\Request as ModelsRequest;
use App\Models\Schedule;
use App\Models\Type;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Spatie\Permission\Models\Role;

class FixController extends ApiController
{
    public function setDefaultScheduleForAllUsers()
    {
        $defaultScheduleId = 1;
        User::whereNotNull('id')->update(['schedule_id' => $defaultScheduleId]);
        dd("done");
    }

    public function getAllRole()
    {
        $user = auth('sanctum')->user();
        return $user->roles->pluck('id');
    }

    public function sendMail($userID)
    {
        $user = User::find($userID);
        $request = ModelsRequest::find(65);
        $type = Type::find(6);
        Mail::to($user->email)->send(new NewRequestFinalApproveMail($request, $user->name, $type, $user->id));
        return true;
    }

    public function testTimeZone(Request $request)
    {
        dd($request->from, $request->to);

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);
        $timezoneString = $request->header('Timezone');
        $timezone = new DateTimeZone($timezoneString);
        $offsetHours = $timezone->getOffset($from) / 3600;

        $from = $from->addHours($offsetHours);
        $start_request = $from->format('Y-m-d H:i');
        $to = $to->addHours($offsetHours);
        $end_request = $to->format('Y-m-d H:i');
    }

    /*public function calculateHours(Request $request)
    {
        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);
        $user = User::find($request->user_id);

        //Choose an Rnd Time before or same for Start Time
        if ($to->lte($from)) {
            return null; // Invalid time input
        }

        $holidays = $this->getHolidayDayNameForUser($user);
        $userSchedule = Schedule::find($user->schedule_id);
        $startSchedule = Carbon::parse($userSchedule->start);
        $endSchedule = Carbon::parse($userSchedule->end);
        $totalHours = 0;

        //Request In One Day
        if ($from->isSameDay($to)) {
            if (in_array($from->format('l'), $holidays)) {
                //Request Fall In Holidays
                $totalHours = 0;
            } else {
                $start = max($from, $startSchedule);
                $end = min($to, $endSchedule);
                if ($start->gt($to)) {
                    //Request Out Of Schedule
                    $totalHours = 0;
                } else {
                    $totalHours = abs($end->diffInMinutes($start)) / 60;
                }
            }
        }
        //Request Has Multible Days
        else {
            $currentDate = $from;
            while ($currentDate->lt($to)) {
                // Check if the current date is a holiday
                if (!in_array($currentDate->format('l'), $holidays)) {
                    // Calculate hours for the current day within schedule hours
                    $startSchedule = new Carbon($currentDate->format('y-m-d') . ' ' . $userSchedule->start);
                    $endSchedule = new Carbon($currentDate->format('y-m-d') . ' ' . $userSchedule->end);
                    if ($startSchedule->lt($currentDate)) {
                        $start = $startSchedule;
                    } else {
                        $start = max($currentDate, $startSchedule);
                    }
                    $end = min($to, $endSchedule);
                    if ($start->lt($end)) {
                        $hours = abs($end->diffInMinutes($start)) / 60;
                        $totalHours += $hours;
                    }
                }
                $currentDate->addDay();
            }
        }
        return $totalHours;
    }*/

    public function calculateHours(Request $request)
    {
        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);
        $user = User::find($request->user_id);

        // Validate time input
        if ($to->lte($from)) {
            return null; // Invalid time input
        }

        $holidays = $this->getHolidayDayNameForUser($user);
        $userSchedule = Schedule::find($user->schedule_id);
        $startSchedule = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->start);
        $endSchedule = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->end);
        $totalHours = 0;

        // Calculate total hours for the given date range
        while ($from->lt($to)) {
            // Check if the current date is not a holiday
            if (!in_array($from->format('l'), $holidays)) {
                // Calculate hours for the current day within schedule hours

                $startSchedule = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->start);
                $endSchedule = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->end);
                if (!$startSchedule->lte($from)) {
                    $start = $startSchedule;
                } else {
                    $start = max($from, $startSchedule);
                }
                $end = min($to, $endSchedule);
                if ($start->lt($end)) {
                    $totalHours += $start->diffInMinutes($end) / 60;
                }
            }
            $from->addDay();
            $from = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->start);
        }

        // dd($to, $endSchedule);
        // dd($to, $from);
        // if ($to->lt($from)) {
        //     dd("less");
        // }
        return $totalHours;
    }

    private function getHolidayDayNameForUser($user)
    {
        $schedule = Schedule::find($user->schedule_id);
        $holidays = $schedule->ScheduleHolidays;
        $dayNames = $holidays->pluck('day')->toArray();
        return $dayNames;
    }


    //Calculate the amount of hours of the request Overtime
    public function calculateOvertimeHours(Request $request)
    {
        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);
        $user = User::find($request->user_id);
        $type = $request->type;

        // Check if "to" is greater than "from" 
        if ($to->lte($from)) {
            return null; // Invalid time input
        }

        $holidays = $this->getHolidayDayNameForUser($user);

        $userSchedule = Schedule::find($user->schedule_id);
        // $scheduleStart = new Carbon($from->format('y-m-d') . ' ' . $userSchedule->start);
        // $scheduleEnd = new Carbon($from->format('y-m-d') . ' ' . $userSchedule->end);
        $totalHours = 0;

        if ($type === 'Overtime at Company on Holiday') {
            // Calculate the hours only for the holiday days
            while ($from->lte($to)) {
                if (in_array($from->format('l'), $holidays)) {
                    $end = $from->copy()->addDay()->startOfDay();
                    $totalHours += abs($from->diffInMinutes($end) / 60);
                }
                $from->addDay();
            }
        } elseif ($type === 'Overtime at Company') {
            if (!$from->isSameDay($to)) {
                dd("At Company Must be in the same day");
            }
            // Calculate the hours for non-holiday days outside the user's schedule
            if (!in_array($from->format('l'), $holidays) && !$this->isNonWorkingDay($from, $holidays)) {
                $schedule_start = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->start);
                $schedule_end = Carbon::parse($from->format('y-m-d') . ' ' . $userSchedule->end);
                if ($from->lt($schedule_start)) {
                    if ($to->lt($schedule_start)) {
                        $totalHours += abs($from->diffInMinutes($to) / 60);
                    }
                    $totalHours += abs($from->diffInMinutes($schedule_start) / 60);
                    if ($to->gt($schedule_end)) {
                        $totalHours += abs($schedule_end->diffInMinutes($to) / 60);
                    }
                } else {
                    ////////here
                    $totalHours += abs($schedule_end->diffInMinutes($to) / 60);
                }
            }
        } elseif ($type === 'Overtime at Home') {
            // Calculate the hours for days outside the user's schedule or during vacation with status final_approve
            $currentDate = $from->copy();
            while ($currentDate->lte($to)) {
                if (!$this->isNonWorkingDay($currentDate, $holidays) || $this->isVacationFinalApproved($currentDate, $user)) {
                    if ($currentDate->isSameDay($from)) {
                        $start = max($from, $fromEndTime);
                    } else {
                        $start = $fromStartTime;
                    }
                    $end = $to;
                    $hours = $start->diffInHours($end);
                    $hours += abs(($end->minute - $start->minute) / 60);

                    $totalHours += $hours;
                }
                $currentDate->addDay();
            }
        }
        return $totalHours;
    }

    private function isNonWorkingDay($date, $holidays)
    {
        $date = $date->format('l');
        if (in_array($date, $holidays)) {
            return true;
        }
        return false;
    }

    //Old Calculate Hours
    /*
    private function calculateHours($from, $to, $user)
    {
        // Check if "to" is greater than "from" 
        if ($to->lt($from)) {
            return null; // Invalid time input
        }
        $holidays = $this->getHolidayDayNameForUser($user);

        $userSchedule = Schedule::find($user->schedule_id);
        $startSchedule = new Carbon($from->format('y-m-d') . ' ' . $userSchedule->start);
        $endSchedule = new Carbon($to->format('y-m-d') . ' ' . $userSchedule->end);
        $totalHours = 0;

        $fromStartTime = new Carbon($from);
        $fromEndTime = new Carbon($to);


        if ($from->isSameDay($to)) {
            if (!in_array($from->format('l'), $holidays)) {
                // Get the start and end times of the schedule
                $start = max($from, $startSchedule); // Consider the later time as the start time
                //(ex: make a request from 07:00 but the schedule of the user start at 08:30 we will get the max between 07:00 and 08:30)

                $end = min($to, $fromEndTime); // Consider the earlier time as the end time
                //(ex: make a request to 17:00 but the schedule of the user end at 16:30 we will get the min between 17:00 and 16:30)

                $hours = abs($end->diffInMinutes($start)) / 60;
                return $hours;
            }
            return null;
        }

        //calculate the hours for the From
        if (!in_array($from->format('l'), $holidays)) {
            $start = max($from->format('H:i:s'), $fromStartTime->format('H:i:s'));
            $end = new Carbon($from->format('y-m-d') . ' ' . $endSchedule->format('H:i:s'));
            $start = new Carbon($from->format('y-m-d') . ' ' . $start);
            $hours = $end->diffInHours($start);
            $hours += abs(($end->minute - (Carbon::parse($start)->minute)) / 60);
            $totalHours += $hours;
        }

        //calculate the hours for the To
        if (!in_array($to->format('l'), $holidays)) {
            $end = min($to->format('H:i:s'), $fromEndTime->format('H:i:s'));
            $end = new Carbon($to->format('y-m-d') . ' ' . $end);
            $start = new Carbon($to->format('y-m-d') . ' ' . $startSchedule->format('H:i:s'));
            $hours = $end->diffInHours($start);
            $hours += abs(((Carbon::parse($end)->minute) - $start->minute) / 60);
            $totalHours += $hours;
        }

        //calculate the hours for the days between From and To
        $currentDate = $from->copy()->addDay();
        $to = $to->subDay();
        while ($currentDate->lt($to)) {
            // Check if the current date is a holiday
            if (!in_array($currentDate->format('l'), $holidays)) {
                $hours = $fromStartTime->diffInHours($fromEndTime);
                $hours += abs(($fromEndTime->minute - $fromStartTime->minute) / 60);
                $totalHours += $hours;
            }
            $currentDate->addDay();
        }
        return $totalHours;
    }
    */
}
