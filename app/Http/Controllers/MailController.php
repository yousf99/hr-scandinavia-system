<?php

namespace App\Http\Controllers;

use Mail;
use App\Mail\RejectRequestMail;
use App\Mail\ResetPasswordMail;
use App\Mail\SetPasswordMail;
use Carbon\Carbon;

class MailController extends Controller
{
    static function reject_request($note, $adminName, $from, $to, $type, $user_email)
    {
        $mailData = [
            'title' => 'Request Rejected - Scandinaviatech HRM',
            'note' => $note,
            'adminName' => $adminName,
            'from' => $from,
            'to' => $to,
            'type' => $type,
        ];

        Mail::to($user_email)->send(new RejectRequestMail($mailData));
    }

    static function setPassword($user_id, $user_email, $secureToken)
    {
        $mailData = [
            'title' => 'Set Password - ScandinaviaTech HRM',
            'secure_token' => $secureToken,
            'email' => $user_email,
            'user_id' => $user_id,
            // 'expiry' => Carbon::now()->addMinutes(20)->toDateTimeString(),
        ];

        Mail::to($user_email)->send(new SetPasswordMail($mailData));
    }

    public static function resetPassword($user_id, $userEmail, $secureToken)
    {
        $mailData = [
            'title' => 'Reset Password - ScandinaviaTech HRM',
            'secure_token' => $secureToken,
            'email' => $userEmail,
            'user_id' => $user_id,
            'expiry' => Carbon::now()->addMinutes(20)->toDateTimeString(),
        ];

        Mail::to($userEmail)->send(new ResetPasswordMail($mailData));
    }
}
