<?php

namespace App\Http\Controllers;

use App\Http\Controllers\api\v1\ApiController;
use App\Http\Requests\attachment\AttachmentStoreRequest;
use App\Models\Attachment;

class AttachmentController extends ApiController
{
    public function store(AttachmentStoreRequest $request)
    {
        $attachments = [];
        foreach ($request->file('attachments') as $file) {
            $attachmentData = $this->upload_file($file);
            $attachmentId = Attachment::insertGetId([
                'url' => $attachmentData['path'],
                'original_name' => $attachmentData['original_name'],
            ]);
            $attachment = [
                'id' => $attachmentId,
                'url' => $attachmentData['path'],
                'original_name' => $attachmentData['original_name'],
            ];
            $attachments[] = $attachment;
        }
        return $this->apiResponse($attachments, ApiController::STATUS_CREATED, 'Files have been uploaded successfully');
    }

    public function destroy(Attachment $attachment)
    {
        //
    }
}
