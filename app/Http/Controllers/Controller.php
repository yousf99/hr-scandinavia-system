<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function sendNotificationToDevice(Request $request)
    {
        //My Device
        // $deviceToken = 'dKzi3zKC1dsguS0nl9nR8I:APA91bFXqyV4DQLJJB1G17yfIHbdAiUUk1ojx1HKuC8HXw0fB6taevRR4UyrfbStcYMperK2prFPsGHo59E62uwxB5hAhRVppemSScYpYsjptvFIk81ckJd8SzxmsBnedzvPvcD0XnP4';
        //Home Device
        // $deviceToken = 'eAiFTa72tgI7BRm-2n3Xqc:APA91bETwAk3ewa5GC8Dpf3wyhQzFFv4rzhEGm4an34qFarnfHjQLQVKS3r0fQFQMmZMnPg01Plphw8WZe0XlRHyrwAHhXc6lfGyBFJeXeh19r0zeGl1THwDe3d3MNIH5qJFWCoHgNzu';
        //Belal Device 
        //$deviceToken = 'e3w1EM6hrPd8kM5F7JVpc1:APA91bHGrHu9KJqra8WSbTOniN16es8ayTHcic20Hhyc9V1FnB71e9huL1t2_DvW36NhYYRFn0BuHu3U3dUUycla-CGvdh6LmSeBKyXYNYKciU7Vg43sz9Gt9YLcnT8YzECBTys2UKWN';
        //My Mobile 
        // $deviceToken = 'fk3rSu4Pz66gcVKshps-uR:APA91bGES6DThRcsK3c--JIJryvOiVmgsgBNe9LxfNi4WFPVRIiFoHz8UQUmU4nHjn9vMkHh0NcoKJdvsqkFrH9jljn2ZgBAin54Paf10Zb8b-3iabfI1-zKRnO0IT5kH1JuksyJstmn';
        //Osama Mobile
        // $deviceToken = 'dl4OjTtheA3g8wV-bmSCbT:APA91bGg0GIoGh1xnwjenD8_QRgz2OTZX6RNrOHJs5UzeG0O1Zagpv0alXJX09CCfxT4Rr57JLoJn-Cc3O0nawOA9_LoWAwFZl_sI8cTs7jo6bK4j8eeHNCR8zOt9MD6I_gut8_bH8kJ';

        $deviceToken = $request->deviceToken;
        // Create a new Firebase Factory instance
        $factory = (new Factory)->withServiceAccount(__DIR__ . '/../service-account.json');

        // Create a new Cloud Messaging instance
        $messaging = $factory->createMessaging();

        $iconPath = 'http://hrapi.scandinavia-group.com/storage/images/logo-portrait.png';
        $clickActionUrl = 'http://hr-front.scandinavia-group.com/';

        // Create a new Cloud Message and attach the notification
        $notification = Notification::create('HRMS', 'New Notification From HRMS');

        // Create a new Cloud Message and attach the notification and custom parameters
        $message = CloudMessage::withTarget('token', $deviceToken)
            ->withNotification($notification)
            ->withWebPushConfig([
                'notification' => [
                    'icon' => $iconPath,
                    'click_action' => $clickActionUrl,
                ],
            ]);

        // Send the message using Firebase Cloud Messaging
        $messaging->send($message);

        return response()->json(['message' => 'Notification sent successfully']);
    }
}
