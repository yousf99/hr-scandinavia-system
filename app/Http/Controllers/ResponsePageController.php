<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponsePageController extends Controller
{
    public function approvalPage()
    {
        return view('approval_page');
    }

    public function rejectPage()
    {
        return view('rejecte_page');
    }
}
