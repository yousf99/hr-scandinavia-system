<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->created_at,
            'title' => $this->title,
            'body' => $this->body,
            // 'url' => $this->url
            'is_read' => $this->is_read,
            'read_at' => $this->read_at,
            // 'user' => new AuthResource($this->User),
            'actions' => ActionResource::collection($this->Actions),
        ];
    }
}
