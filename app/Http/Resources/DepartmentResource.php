<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            // 'number_of_user' => $this->NumberOfUsers(),
            'number_of_users' => $this->whenAppended('number_of_users', $this->NumberOfUsers()),
        ];
    }
}
