<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserRequestsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'from' => $this->from,
            'to' => $this->to,
            'hours' => $this->hours,
            'status' => $this->status,
            'description' => $this->description,
            'note' => $this->note,
            'type' => new TypeResource($this->Type),
        ];
    }
}
