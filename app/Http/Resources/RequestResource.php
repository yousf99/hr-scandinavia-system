<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RequestResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->created_at,
            'from' => $this->from,
            'to' => $this->to,
            'hours' => $this->hours,
            'status' => $this->status,
            'description' => $this->description,
            'note' => $this->note,
            'is_overtime' => $this->is_overtime,
            'project' => new ProjectResource($this->Project),
            'approved_by' => new AuthResource($this->approvedBy),
            'user' => new UserResource($this->whenLoaded('User')),
            'type' => new TypeResource($this->Type),
        ];
    }
}
