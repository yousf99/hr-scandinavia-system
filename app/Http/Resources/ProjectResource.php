<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'status' => $this->status,
            'department' => new DepartmentResource($this->Department),
            'branch' => new BranchResource($this->Branch),
            'users' => AuthResource::collection($this->whenLoaded('Users')),
            'attachments' => AttachmentResource::collection($this->whenLoaded('Attachments')),
        ];
    }
}
