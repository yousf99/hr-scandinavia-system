<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectUsersResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'project' => new ProjectResource($this->Project),
            'users' => UserResource::collection($this->User),
        ];
    }
}
