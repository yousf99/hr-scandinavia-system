<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserBalanceResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'balance' => new BalanceResource($this->Balance),
            'amount' => $this->amount,
        ];
    }
}
