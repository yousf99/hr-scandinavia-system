<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone' => $this->phone,
            'position' => $this->position,
            'image' => $this->getFullPath(),
            'personal_email' => $this->personal_email,
            'address' => $this->address,
            'description' => $this->description,
            'start_date' => $this->start_date,
            'student' => $this->is_student,
            'balances' => UserBalanceResource::collection($this->UserBalances),
            'department' => new DepartmentResource($this->Department),
            'branch' => new BranchResource($this->Branch),
            'attachments' => AttachmentResource::collection($this->whenLoaded('Attachments')),
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
            'schedule' => new ScheduleResource($this->Schedule),

        ];
    }
}
