<?php

namespace App\Http\Requests\balance;

use Illuminate\Foundation\Http\FormRequest;

class BalanceUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return ['name' => 'string'];
    }
}
