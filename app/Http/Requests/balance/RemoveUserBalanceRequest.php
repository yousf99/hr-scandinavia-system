<?php

namespace App\Http\Requests\balance;

use Illuminate\Foundation\Http\FormRequest;

class RemoveUserBalanceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'balance_id' => 'required|exists:balances,id',
        ];
    }
}
