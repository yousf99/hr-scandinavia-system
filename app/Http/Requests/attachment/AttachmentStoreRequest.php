<?php

namespace App\Http\Requests\attachment;

use Illuminate\Foundation\Http\FormRequest;

class AttachmentStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'attachments' => 'array',
            'attachments.*' => 'file|mimes:doc,docx,pdf',
        ];
    }
}
