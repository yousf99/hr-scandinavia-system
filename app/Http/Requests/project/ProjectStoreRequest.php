<?php

namespace App\Http\Requests\project;

use App\Enums\ProjectStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class ProjectStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required',
            'description' => 'nullable|string',
            'status' => ['required', new Enum(ProjectStatus::class)],
            'start_date' => 'date',
            'end_date' => 'date',
            'department_id' => 'required|exists:departments,id',
            'branch_id' => 'required|exists:branches,id',
            'user_id' => 'nullable|array',
            'user_id.*' => 'exists:users,id',
            'attachments' => 'array',
            'attachments.*' => 'exists:attachments,id',
        ];
    }
}
