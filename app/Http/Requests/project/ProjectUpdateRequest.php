<?php

namespace App\Http\Requests\project;

use App\Enums\ProjectStatus;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class ProjectUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string',
            'description' => 'string|nullable',
            'status' => [new Enum(ProjectStatus::class)],
            'start_date' => 'date',
            'end_date' => 'date',
            'department_id' => 'exists:types,id',
            'user_id' => 'array',
            'user_id.*' => 'exists:users,id'
        ];
    }
}
