<?php

namespace App\Http\Requests\project;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'exists:users,id',
        ];
    }
}
