<?php

namespace App\Http\Requests\branch;

use Illuminate\Foundation\Http\FormRequest;

class BranchUpdateRequest extends FormRequest
{
    private $branch_id;

    public function __construct($branch_id)
    {
        parent::__construct();
        $this->branch_id = $branch_id;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required|unique:branches,name,' . $this->branch_id,
        ];
    }
}
