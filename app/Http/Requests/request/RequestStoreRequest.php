<?php

namespace App\Http\Requests\request;

use Illuminate\Foundation\Http\FormRequest;

class RequestStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'from' => 'date|required',
            'to' => 'date|required',
            // 'hours' => 'string|required',
            'description' => 'string|required',
            'type_id' => 'required|exists:types,id',
            'project_id' => 'exists:projects,id'
        ];
    }
}
