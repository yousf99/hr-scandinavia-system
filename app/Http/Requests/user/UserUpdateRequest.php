<?php

namespace App\Http\Requests\user;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    private $user_id;

    public function __construct($user_id)
    {
        parent::__construct();
        $this->user_id = $user_id;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'first_name' => 'string',
            'last_name' => 'string',
            'phone' => 'string|regex:/^\+?[0-9]{10,}$/|unique:users,phone,' . $this->user_id,
            'position' => 'string',
            'start_date' => 'date',
            'address' => 'string',
            'email' => 'string|unique:users,email,' . $this->user_id,
            'personal_email' => 'string|unique:users,personal_email,' . $this->user_id,
            'description' => 'string',
            'department_id' => 'exists:departments,id',
            'branch_id' => 'exists:branches,id',
            'attachments' => 'array',
            'attachments.*' => 'exists:attachments,id',
            'schedule_id' => 'exists:schedules,id',
            'image' => 'string'
        ];

        return $rules;
    }
}
