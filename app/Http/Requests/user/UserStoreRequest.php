<?php

namespace App\Http\Requests\user;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'email' => 'string|required|unique:users,email',
            'phone' => 'string|required|regex:/^\+?[0-9]{10,}$/|unique:users,phone',
            'position' => 'string|required',
            'start_date' => 'date|required',
            // 'is_student' => 'string',
            'personal_email' => 'string|required|unique:users,personal_email',
            'address' => 'string|required',
            'attachments' => 'array',
            'attachments.*' => 'exists:attachments,id',
            'department_id' => 'required|exists:departments,id',
            'branch_id' => 'required|exists:branches,id',
            // 'schedule_id' => 'required|exists:schedules,id',
            'image' => 'string'
        ];
    }
}
