<?php

namespace App\Http\Requests\auth;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user_id = auth('sanctum')->user()->id;
        return [
            'first_name' => 'string',
            'last_name' => 'string',
            'personal_email' => 'string|unique:users,personal_email,' . $user_id,
            'phone' => 'string|regex:/^\+?[0-9]{10,}$/|unique:users,phone,' . $user_id,
            'image' => 'string',
            'address' => 'string',
        ];
    }
}
