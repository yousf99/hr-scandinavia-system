<?php

namespace App\Http\Requests\auth;

use Illuminate\Foundation\Http\FormRequest;

class DeviceTokenRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'device_token' => 'string|required',
        ];
    }
}
