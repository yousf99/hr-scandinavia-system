<?php

namespace App\Http\Requests\type;

use Illuminate\Foundation\Http\FormRequest;

class TypeStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required|unique:types,name',
            'factor' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) {
                    if ($value < -2 || $value > 2) {
                        $fail($attribute . ' must be between -2 and +2');
                    }
                },
                'max:50',
            ],
            'balance_id' => 'exists:balances,id',
        ];
    }
}
