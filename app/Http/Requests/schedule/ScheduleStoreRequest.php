<?php

namespace App\Http\Requests\schedule;

use App\Enums\Weekdays;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class ScheduleStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'start' => ['required', 'date'],
            'end' => ['required', 'date'],
            'holiday_days' => ['array'],
            'holiday_days.*' => [new Enum(Weekdays::class)],
            // 'holiday_days.*' => [Rule::in(Weekdays::getAllValues(), 'i')],
        ];
    }
}
