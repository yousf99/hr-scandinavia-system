<?php

namespace App\Http\Requests\position;

use Illuminate\Foundation\Http\FormRequest;

class PositionUpdateRequest extends FormRequest
{
    private $position_id;

    public function __construct($position_id)
    {
        parent::__construct();
        $this->position_id = $position_id;
    }

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required|unique:positions,name,' . $this->position_id,
            'description' => 'string',
        ];
    }
}
