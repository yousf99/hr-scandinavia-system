<?php

namespace App\Http\Requests\position;

use Illuminate\Foundation\Http\FormRequest;

class PositionStoreRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required|unique:positions,name',
            'description' => 'string',
        ];
    }
}
