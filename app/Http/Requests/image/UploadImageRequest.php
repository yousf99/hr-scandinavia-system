<?php

namespace App\Http\Requests\image;

use Illuminate\Foundation\Http\FormRequest;

class UploadImageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'image' => 'required|image|mimes:png,jpg,jpeg,webp|max:2048', // Maximum size: 2 MB (2048 KB)
        ];
    }

    // public function messages()
    // {
    //     return [
    //         'image.required' => 'The image is required.',
    //         'image.image' => 'The file must be an image.',
    //         'image.mimes' => 'The image must be a PNG, JPG, JPEG, or WEBP file.',
    //         'image.max' => 'The image size must not exceed 2 MB.',
    //     ];
    // }
}
