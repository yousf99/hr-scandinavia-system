<?php

namespace App\Http\Requests\role;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string',
            'permissions' => 'array',
            'permissions.*' => 'exists:permissions,id',
        ];
    }
}
