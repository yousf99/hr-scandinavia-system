<?php

namespace App\Http\Requests\role;

use Illuminate\Foundation\Http\FormRequest;

class StoreRoleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'string|required',
            'permissions' => 'array',
            'permissions.*' => 'exists:permissions,id',
        ];
    }
}
