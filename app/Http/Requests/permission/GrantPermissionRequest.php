<?php

namespace App\Http\Requests\permission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class GrantPermissionRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'permissions' => ['required', 'exists:permissions,name']
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
