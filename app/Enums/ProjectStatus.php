<?php

namespace App\Enums;

enum ProjectStatus: string
{
    case pending = 'pending';
    case in_progress = 'in_progress';
    case done = 'done';

    public static function getAllValues(): array
    {
        return array_column(ProjectStatus::cases(), 'value');
    }
}
