<?php

namespace App\Enums;


enum RequestStatus: string
{
    case pending = 'pending';
    case first_approved = 'first_approved';
    case final_approved = 'final_approved';
    case rejected = 'rejected';

    public static function getAllValues(): array
    {
        return array_column(RequestStatus::cases(), 'value');
    }
}
