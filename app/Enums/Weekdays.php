<?php

namespace App\Enums;

enum Weekdays: string
{
    case SUNDAY = 'Sunday';
    case MONDAY = 'Monday';
    case TUESDAY = 'Tuesday';
    case WEDNESDAY = 'Wednesday';
    case THURSDAY = 'Thursday';
    case FRIDAY = 'Friday';
    case SATURDAY = 'Saturday';

    public static function getAllValues(): array
    {
        return array_column(Weekdays::cases(), 'value');
    }
}
