<?php

namespace App\Sort;

use Closure;
use Illuminate\Database\Eloquent\Builder;

class ProjectSort
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (is_array($query)) {
            $query = (object)$query;
        }
        //default sort by id desc
        if (empty($query->sort)) {
            $query->sort = '-id';
        }
        $builder = $next($request);
        $sortField = $query->sort;
        $sortOrder = 'asc';

        // if the sort value starts with `-`, sortOrder will be desc
        if (substr($sortField, 0, 1) === '-') {
            $sortOrder = 'desc';
            $sortField = ltrim($sortField, '-');
        }

        if ($sortField === 'date') {
            return $builder->orderBy('created_at', $sortOrder);
        } elseif ($sortField === 'department.name') {
            return $builder
                ->select('projects.*', 'departments.name as department_name')
                ->orderBy('department_name', $sortOrder)
                ->join('departments', 'projects.department_id', '=', 'departments.id');
        } elseif ($sortField === 'branch.name') {
            return $builder
                ->select('projects.*', 'branches.name as branch_name')
                ->orderBy('branch_name', $sortOrder)
                ->join('branches', 'projects.branch_id', '=', 'branches.id');
        } else {
            return $builder->orderBy($sortField, $sortOrder);
        }
    }
}
