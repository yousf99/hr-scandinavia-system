<?php

namespace App\Sort;

use Closure;
use Illuminate\Database\Eloquent\Builder;

class RequestSort
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (is_array($query)) {
            $query = (object)$query;
        }
        //default sort by id desc
        if (empty($query->sort)) {
            $query->sort = '-id';
        }
        $builder = $next($request);
        $sortField = $query->sort;
        $sortOrder = 'asc';

        // if the sort value start with `-` sortOrder will be desc
        if (substr($sortField, 0, 1) === '-') {
            $sortOrder = 'desc';
            $sortField = ltrim($sortField, '-');
        }

        if ($sortField === 'date') {
            return $builder->orderBy('created_at', $sortOrder);
        } elseif ($sortField === 'user.department.name') {
            return $builder->select('requests.*')
                ->join('users', 'requests.user_id', '=', 'users.id')
                ->join('departments', 'users.department_id', '=', 'departments.id')
                ->orderBy('departments.name', $sortOrder)
                ->orderBy('requests.id');
        } elseif ($sortField === 'user.first_name') {
            return $builder->orderBy('users.first_name', $sortOrder)
                ->orderBy('users.last_name', $sortOrder)
                ->join('users', 'requests.user_id', '=', 'users.id')
                ->select('requests.*');
        } else {
            return $builder->orderBy($sortField, $sortOrder);
        }
    }
}
