<?php

namespace App\Sort;

use Closure;
use Illuminate\Database\Eloquent\Builder;

class NotificationSort
{
    public function handle($request, Closure $next)
    {
        $query = json_decode(json_encode(request()->query()));

        if (is_array($query)) {
            $query = (object)$query;
        }
        //default sort by id desc
        if (empty($query->sort)) {
            $query->sort = '-id';
        }
        $builder = $next($request);
        $sortField = $query->sort;
        $sortOrder = 'asc';

        // if the sort value start with `-` sortOrder will be desc
        if (substr($sortField, 0, 1) === '-') {
            $sortOrder = 'desc';
            $sortField = ltrim($sortField, '-');
        }

        if ($sortField === 'date') {
            return $builder->orderBy('created_at', $sortOrder);
        } else {
            return $builder->orderBy($sortField, $sortOrder);
        }
    }
}
