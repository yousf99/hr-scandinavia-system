<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Request;

class NewRequestFirstApproveMail extends Mailable
{
    use Queueable, SerializesModels;

    public $request;
    public $mailData;

    public function __construct($request, $employee_name, $position, $department, $type, $admin_id)
    {
        $this->request = $request;
        $this->mailData = [
            'request_type' => $type,
            'employee' => $employee_name,
            'from' => $request->from,
            'to' => $request->to,
            'total_hours' => $request->hours,
            'admin_id' => $admin_id,
            'position' => $position,
            'department' => $department,
            'time' => $request->created_at
        ];
    }

    public function build()
    {
        return $this->view('emails.new_request_first_approve')
            ->subject('New Request First Approval');
    }
}
