<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RejectRequestMail extends Mailable
{
    use Queueable, SerializesModels;

    public $mailData;

    public function __construct($note, $adminName, $from, $to, $type)
    {
        $this->mailData = [
            'note' => $note,
            'adminName' => $adminName,
            'from' => $from,
            'to' => $to,
            'employee' => "test user",
            'total_hours' => 10,
            'type' => $type,
        ];
    }

    public function build()
    {
        return $this->subject('Request Rejected')
            ->view('emails.rejectedRequestMail')
            ->with($this->mailData);
    }
}
