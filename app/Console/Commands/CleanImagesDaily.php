<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CleanImagesDaily extends Command
{
    protected $signature = 'clean-images:daily';

    protected $description = 'Deleting the images that is not related to any user';

    protected $excludedImages = [
        'logo-portrait.png',
        'user-avatar.png',
    ];

    public function handle()
    {
        $usersWithImages = User::whereNotNull('image')->pluck('image')->toArray();

        $files = Storage::disk('public')->files('images');

        foreach ($files as $file) {
            if (!in_array($file, $usersWithImages) && !$this->isExcludedImage($file)) {
                Storage::disk('public')->delete($file);
                $this->info('Deleted: ' . $file);
            }
        }

        $this->info('Unused images deleted successfully.');
    }

    protected function isExcludedImage($file)
    {
        $filename = pathinfo($file, PATHINFO_BASENAME);
        return in_array($filename, $this->excludedImages);
    }
}
