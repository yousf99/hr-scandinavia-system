<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\UserBalance;


class AddExamVacation6Month extends Command
{
    protected $signature = 'exam-vacation:6month';

    protected $description = 'Add 6 days to exam vacations of student employee every semester';

    public function handle()
    {
        $users = User::where('is_student', 1)->get();
        foreach ($users as $user) {
            $user_balance = UserBalance::where('user_id', $user->id)->where('balance_id', 2)->first();
            $user_balance->amount = 0;
            $user_balance->save();
            User::AddToAmount($user->id, 48, 2);
        }
        $this->info('Successfully Add Exam Vacation Amount To Student Employees');
    }
}