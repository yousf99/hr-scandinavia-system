<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\UserBalance;

class AddPaidVacationMonthly extends Command
{
    protected $signature = 'paid-vacation:monthly';

    protected $description = 'Add 8 hours to balance of paid vacations for every employees except the new employees every month';

    public function handle()
    {
        $users = User::all();

        foreach ($users as $user) {
            // Find the user's balance
            $user_balance = UserBalance::where('balance_id', 1)->where('user_id', $user->id)->first();

            if ($user_balance === null) {
                // Create a new balance record if not found
                $user_balance = UserBalance::create([
                    'user_id' => $user->id,
                    'balance_id' => 1,
                    'amount' => 0
                ]);
            }

            // Calculate and add to the balance
            $amountToAdd = $user->AddToPaidVacation();
            $user_balance->amount += $amountToAdd;
            $user_balance->save();
        }

        $this->info('Successfully added paid vacation to employees');
    }
}
