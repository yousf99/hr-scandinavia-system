<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\AddPaidVacationMonthly::class,
        Commands\AddExamVacation6Month::class,
        Commands\CleanImagesDaily::class,
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('paid-vacation:monthly')->monthlyOn(1);
        $schedule->command('exam-vacation:6month')->yearlyOn(1, 1);
        $schedule->command('exam-vacation:6month')->yearlyOn(6, 1);
        $schedule->command('clean-images:daily')->daily();
    }


    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
