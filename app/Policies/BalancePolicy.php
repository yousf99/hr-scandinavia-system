<?php

namespace App\Policies;

use App\Models\Balance;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BalancePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('balance_access') || $user->can('balance_control');
    }

    public function show(User $user, Balance $balance)
    {
        return $user->can('balance_access') || $user->can('balance_control');
    }

    public function store(User $user)
    {
        return $user->can('balance_control') ? true : false;
    }

    public function update(User $user, Balance $balance)
    {
        return $user->can('balance_control') ? true : false;
    }

    public function addBalanceToUser(User $user)
    {
        return $user->can('balance_control') ? true : false;
    }

    public function removeBalanceFromUser(User $user)
    {
        return $user->can('balance_control') ? true : false;
    }

    public function updateUserBalance(User $user)
    {
        return $user->can('balance_control') ? true : false;
    }
}
