<?php

namespace App\Policies;

use App\Models\Type;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TypePolicy
{
    use HandlesAuthorization;

    public function show(User $user, Type $type)
    {
        return $user->can('type_access') || $user->can('type_control');
    }

    public function store(User $user)
    {
        return $user->can('type_control') ? true : false;
    }

    public function update(User $user, Type $type)
    {
        return $user->can('type_control') ? true : false;
    }

    public function softDelete(User $user, Type $type)
    {
        return $user->can('type_control') ? true : false;
    }
}
