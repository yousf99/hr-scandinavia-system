<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Schedule;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchedulePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('schedule_access') || $user->can('schedule_control');
    }

    public function show(User $user, Schedule $schedule)
    {
        return $user->can('schedule_access') || $user->can('schedule_control');
    }

    public function store(User $user)
    {
        return $user->can('schedule_control') ? true : false;
    }

    // public function update(User $user, Schedule $schedule)
    // {
    //     return $user->can('schedule_control') ? true : false;
    // }
}
