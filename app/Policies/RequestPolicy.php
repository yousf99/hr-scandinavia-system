<?php

namespace App\Policies;

use App\Enums\RequestStatus;
use App\Models\Request;
use App\Models\User;
use App\Models\Department;
use Illuminate\Auth\Access\HandlesAuthorization;

use Illuminate\Http\Request as HttpRequest;

include_once app_path('constants.php');

class RequestPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('request_access') ? true : false;
    }

    // public function approveRequest(User $user, Request $request)
    // {
    //     $development_department = Department::where('name', 'Development')->first();
    //     $social_media_department = Department::where('name', 'Social Media')->first();

    //     $employee = User::find($request->user_id);

    //     //Development
    //     if ($employee->department_id === $development_department->id) {
    //         //Vacation
    //         if (in_array($request->type_id, TYPE_IDS['vacation'])) {
    //             if ($request->status === RequestStatus::pending->value) {
    //                 if ($user->can('first_approve_vacation_development')) {
    //                     return true;
    //                 }
    //                 $first_approve_vacation_development = User::permission('first_approve_vacation_development')->exists();
    //                 if (!$first_approve_vacation_development) {
    //                     if ($user->can('final_approve_request')) {
    //                         return true;
    //                     }
    //                     return false;
    //                 }
    //                 return false;
    //             } else if ($request->status === RequestStatus::first_approved->value) {
    //                 if ($user->can('final_approve_request')) {
    //                     return true;
    //                 }
    //                 return false;
    //             }
    //         }
    //         //Overtime
    //         if (in_array($request->type_id, TYPE_IDS['overtime'])) {
    //             if ($request->status === RequestStatus::pending->value) {
    //                 if ($user->can('first_approve_overtime_development')) {
    //                     return true;
    //                 }
    //                 $first_approve_overtime_development = User::permission('first_approve_overtime_development')->exists();
    //                 if (!$first_approve_overtime_development) {
    //                     if ($user->can('final_approve_request')) {
    //                         return true;
    //                     }
    //                     return false;
    //                 }
    //                 return false;
    //             } else if ($request->status === RequestStatus::first_approved->value) {
    //                 if ($user->can('final_approve_request')) {
    //                     return true;
    //                 }
    //                 return false;
    //             }
    //         }
    //     }

    //     //Social Media
    //     if ($employee->department_id === $social_media_department->id) {
    //         //Vacation
    //         if (in_array($request->type_id, TYPE_IDS['vacation'])) {
    //             if ($request->status === RequestStatus::pending->value) {
    //                 if ($user->can('first_approve_vacation_social_media')) {
    //                     return true;
    //                 }
    //                 $first_approve_vacation_social_media = User::permission('first_approve_vacation_social_media')->exists();
    //                 if (!$first_approve_vacation_social_media) {
    //                     if ($user->can('final_approve_request')) {
    //                         return true;
    //                     }
    //                     return false;
    //                 }
    //                 return false;
    //             } else if ($request->status === RequestStatus::first_approved->value) {
    //                 if ($user->can('final_approve_request')) {
    //                     return true;
    //                 }
    //                 return false;
    //             }
    //         }
    //         //Overtime
    //         if (in_array($request->type_id, TYPE_IDS['overtime'])) {
    //             if ($request->status === RequestStatus::pending->value) {
    //                 if ($user->can('first_approve_overtime_social_media')) {
    //                     return true;
    //                 }
    //                 $first_approve_overtime_social_media = User::permission('first_approve_overtime_social_media')->exists();
    //                 if (!$first_approve_overtime_social_media) {
    //                     if ($user->can('final_approve_request')) {
    //                         return true;
    //                     }
    //                     return false;
    //                 }
    //                 return false;
    //             } else if ($request->status === RequestStatus::first_approved->value) {
    //                 if ($user->can('final_approve_request')) {
    //                     return true;
    //                 }
    //                 return false;
    //             }
    //         }
    //     }
    // }

    public function approveRequest(User $user, Request $request)
    {
        if (in_array($request->type_id, TYPE_IDS['overtime'])) {
            $type = "overtime";
        } else {
            $type = "vacation";
        }
        $employee = User::find($request->user_id);
        $department = Department::find($employee->department_id);
        $department_name = strtolower(str_replace(' ', '_', $department->name));
        $permissionName = "first_approve_{$type}_{$department_name}";

        if ($request->status === RequestStatus::pending->value) {
            if ($user->can($permissionName)) {
                return true;
            }

            $firstApprovePermissionExists = User::permission($permissionName)->exists();

            if (!$firstApprovePermissionExists) {
                if ($user->can('final_approve_request')) {
                    return true;
                }
                return false;
            }
            return false;
        } elseif ($request->status === RequestStatus::first_approved->value) {
            if ($user->can('final_approve_request')) {
                return true;
            }
            return false;
        }
    }

    public function rejectRequest(User $user, Request $request)
    {
        return $this->approveRequest($user, $request);
    }

    public function addRequestByAdmin(User $user)
    {
        return $user->can('add_request_by_admin') ? true : false;
    }
}
