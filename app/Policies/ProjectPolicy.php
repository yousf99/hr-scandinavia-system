<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('project_access') || $user->can('project_control');
    }

    public function show(User $user, Project $project)
    {
        return $user->can('project_access') || $user->can('project_control');
    }

    public function store(User $user)
    {
        return $user->can('project_control') ? true : false;
    }

    public function update(User $user, Project $project)
    {
        return $user->can('project_control') ? true : false;
    }

    public function destroy(User $user, Project $project)
    {
        return $user->can('project_control') ? true : false;
    }

    public function addUserToProject(User $user, Project $project)
    {
        return $user->can('project_control') ? true : false;
    }

    public function removeUserFromProject(User $user, Project $project)
    {
        return $user->can('project_control') ? true : false;
    }
}
