<?php

namespace App\Policies;

use Spatie\Permission\Models\Permission;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('role_access') || $user->can('role_control');
    }

    public function show(User $user)
    {
        return $user->can('role_access') || $user->can('role_control');
    }

    public function grant(User $user)
    {
        return $user->can('role_control') ? true : false;
    }

    public function revoke(User $user)
    {
        return $user->can('role_control') ? true : false;
    }

    public function update(User $user)
    {
        return $user->can('role_control') ? true : false;
    }
}
