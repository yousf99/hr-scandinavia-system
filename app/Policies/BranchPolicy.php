<?php

namespace App\Policies;

use App\Models\Branch;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('branch_access') || $user->can('branch_control');
        // return true;
    }

    public function store(User $user)
    {
        return $user->can('branch_control') ? true : false;
    }

    public function update(User $user)
    {
        return $user->can('branch_control') ? true : false;
    }
}
