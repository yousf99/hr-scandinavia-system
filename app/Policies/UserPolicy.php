<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        // return $user->can('user_index') ? true : false;
        return $user->can('user_access') || $user->can('user_control');
    }

    public function show(User $user, User $u)
    {
        // return $user->can('user_show') ? true : false;
        return $user->can('user_access') || $user->can('user_control');
    }

    public function store(User $user)
    {
        return $user->can('user_control') ? true : false;
    }

    public function update(User $user)
    {
        return $user->can('user_control') ? true : false;
    }

    public function softDelete(User $user)
    {
        return $user->can('user_control') ? true : false;
    }

    public function restore(User $user)
    {
        return $user->can('user_control') ? true : false;
    }

    public function makeStudent(User $user)
    {
        return $user->can('user_control') ? true : false;
    }

    public function makeNotStudent(User $user)
    {
        return $user->can('user_control') ? true : false;
    }
}
