<?php

namespace App\Policies;

use App\Models\Position;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PositionPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('position_access') || $user->can('position_control');
    }

    public function store(User $user)
    {
        return $user->can('position_control') ? true : false;
    }

    public function update(User $user)
    {
        return $user->can('position_control') ? true : false;
    }
}
