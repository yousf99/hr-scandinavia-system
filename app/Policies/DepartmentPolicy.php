<?php

namespace App\Policies;

use App\Models\Department;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return $user->can('department_access') ? true : false;
    }

    public function store(User $user)
    {
        return $user->can('department_control') ? true : false;
    }
}
