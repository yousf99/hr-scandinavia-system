<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StatisticsPolicy
{
    use HandlesAuthorization;

    public function attendanceStatistics(User $user)
    {
        return $user->can('view_statistics') ? true : false;
    }

    public function projectsStatistics(User $user)
    {
        return $user->can('view_statistics') ? true : false;
    }

    public function userBalanceStatistics(User $user)
    {
        return $user->can('view_statistics') ? true : false;
    }

    public function userOvertimeStatistics(User $user)
    {
        return $user->can('view_statistics') ? true : false;
    }
}
