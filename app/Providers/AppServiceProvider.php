<?php

namespace App\Providers;

use App\Models\Request;
use App\Models\User;
use App\Observers\RequestObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        // User::observe(UserObserver::class);
        // Request::observe(RequestObserver::class);
    }
}
