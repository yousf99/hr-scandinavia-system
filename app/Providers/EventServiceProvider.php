<?php

namespace App\Providers;

use App\Models\Request;
use App\Models\User;
use App\Observers\RequestObserver;
use App\Observers\UserObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [];

    public function boot()
    {
        // User::observe(UserObserver::class);
        // Request::observe(RequestObserver::class);
    }

    public function shouldDiscoverEvents()
    {
        return false;
    }
}
