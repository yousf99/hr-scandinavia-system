<?php

namespace App\Observers;

use App\Mail\SetPasswordMail;
use App\Mail\ResetPasswordMail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserObserver
{
    public function created(User $user)
    {
        $secureToken = Str::uuid();

        $tokenExpiration = now()->addMinutes(20);
        $user->secure_token = $secureToken;
        $user->token_expires_at = $tokenExpiration;
        $user->save();

        Mail::to($user->email)->send(new SetPasswordMail([
            'title' => 'Set Password - ScandinaviaTech HRM',
            'secure_token' => $secureToken,
            'email' => $user->email,
            'user_id' => $user->id,
            'expiry' => $tokenExpiration->toDateTimeString(),
        ]));
    }

    public function forgetPassword(User $user)
    {
        $secureToken = Str::uuid();
        $tokenExpiresAt = now()->addMinutes(20);

        $user->secure_token = $secureToken;
        $user->token_expires_at = $tokenExpiresAt;
        $user->save();

        Mail::to($user->email)->send(new ResetPasswordMail([
            'title' => 'Reset Password - ScandinaviaTech HRM',
            'secure_token' => $user->secure_token,
            'email' => $user->email,
            'user_id' => $user->id,
        ]));
    }
}
