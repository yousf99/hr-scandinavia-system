<?php

namespace App\Observers;

use App\Models\Request;
use App\Mail\RejectRequestMail;
use Illuminate\Support\Facades\Mail;

class RequestObserver
{
    public function rejected(Request $request)
    {
        // Send the rejection email here
        $note = $request->note;
        $adminName = auth('sanctum')->user()->first_name . " " . auth('sanctum')->user()->last_name;
        $from = $request->from;
        $to = $request->to;
        $type = $request->Type->name;
        $userEmail = $request->User->email;

        Mail::to($userEmail)->send(new RejectRequestMail($note, $adminName, $from, $to, $type));
    }
}
