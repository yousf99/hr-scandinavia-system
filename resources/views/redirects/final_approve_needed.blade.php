<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Request can not be change</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: Arial, sans-serif;
        }

        body {
            background-color: #f2f3f8;
        }

        .container {
            position: relative;
            top: 15vh;
            margin: 0 auto;
            background-color: #ffffff;
            border-radius: 20px;
            padding: 40px;
            box-shadow: 0 6px 18px rgba(0, 0, 0, 0.06);
            text-align: center;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            min-height: 70vh;
            max-height: 70vh;
            min-width: 45vw;
            max-width: 45vw;
            width: 45vw;
        }

        .logo img {
            max-width: 200px;
        }

        .title {
            color: #1e1e2d;
            font-size: 24px;
            font-weight: 700;
        }

        p {
            color: #455056;
            font-size: 16px;
            line-height: 26px;
            margin: 0 0 20px 0;
        }

        .message {
            font-size: 18px;
            font-weight: 700;
            color: rgba(255, 82, 82, 1);
            margin: 20px 0 20px 0;
        }

        .button {
            background-color: #0a588e;
            color: #fff;
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
            border-radius: 10px;
            padding: 14px 0;
            width: 100%;
            transition: background-color 0.3s ease;
        }

        .button:hover {
            background-color: #0e6dab;
        }

        /* Tablets */
        @media (max-width: 768px) {
            .container {
                min-width: 90vw;
                max-width: 90vw;
                width: 90vw;
            }
        }

        /*Mobiles*/
        @media (max-width: 480px) {
            .container {
                min-width: 90vw;
                max-width: 90vw;
                width: 90vw;
                min-height: 45vh;
                max-height: 45vh;
                top: 27.5vh;
            }

            p {
                font-size: 14px;
                line-height: 22px;
            }

            .message {
                font-size: 14px;
            }

            .logo img {
                max-width: 175px;
            }

            .button {
                font-size: 14px;
            }
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="logo">
            <img src="http://hrapi.scandinavia-group.com/storage/images/templates/logo-portrait.webp" alt="Logo" />
        </div>
        <p class="message">You can't Approve - Reject This Request</p>
        <p>You do not have permission to approve or reject the request when its status is First Approve</p>
        <a href="{{ env('FRONT_URL') }}" class="button">Go to HRM Dashboard</a>
    </div>
</body>

</html>