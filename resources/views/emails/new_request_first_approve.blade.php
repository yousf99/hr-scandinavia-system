<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Reject Request</title>
  <style>
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    body {
      font-family: Arial, sans-serif;
      background-color: #f4f4f4;
      display: flex;
      justify-content: center;
      align-items: center;
      min-height: 100vh;
      margin: 0;
    }

    .container {
      background-color: #fff;
      border-radius: 15px;
      box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
      padding: 20px;
      width: 95%;
      max-width: 600px;
    }

    .logo {
      text-align: center;
      margin-bottom: 20px;
    }

    .logo img {
      max-width: 50%;
    }

    .section {
      margin-bottom: 20px;
    }

    .main-title {
      color: rgba(0, 86, 140, 1);
      font-weight: 700;
      font-size: 24px;
      margin-bottom: 10px;
    }

    .main-title .date {
      font-size: 16px;
      margin-left: 10px;
      color: #555;
    }

    .title {
      color: #00568c;
      font-size: 16px;
      font-weight: 600;
      width: 30%;
      display: inline-grid;
    }

    .content-text {
      color: #555;
      font-size: 16px;
      margin-top: 5px;
    }

    .text {
      color: #555;
      font-size: 16px;
      margin-top: 5px;
    }

    .section-row {
      margin-top: 15px;
    }

    hr {
      border: 1px solid #ccc;
      margin: 20px 0;
    }

    .button-group {
      margin-top: 20px;
      text-align: center;
    }

    .button {
      background-color: #00568c;
      color: #fff;
      text-decoration: none;
      font-weight: bold;
      font-size: 18px;
      border: none;
      border-radius: 7.5px;
      padding: 15px 30px;
      display: inline-block;
      cursor: pointer;
      transition: background-color 0.3s ease;
      margin: 0 10px;
    }

    .main-text {
      font-weight: 700;
      color: rgba(0, 86, 140, 1);
    }

    .button.approve {
      background-color: rgba(76, 175, 80, 1);
    }

    .button.reject {
      background-color: rgba(255, 82, 82, 1);
    }

    .button.approve:hover {
      background-color: rgba(92, 199, 98, 1);
    }

    .button.reject:hover {
      background-color: rgba(255, 106, 106, 1);
    }

    .button a {
      color: #fff;
    }

    /* Responsive Styles */
    @media screen and (max-width: 600px) {
      .container {
        padding: 10px;
      }

      .main-title {
        font-size: 20px;
      }

      .title {
        width: 40%;
      }

      .button {
        font-size: 16px;
        padding: 10px 20px;
      }
    }
  </style>
</head>

<body>
  <div class="container">
    <div class="logo">
      <a href="{{ env('FRONT_URL') }}" target="_blank">
        <img src="http://hrapi.scandinavia-group.com/storage/images/templates/logo-v2.png" alt="Logo" />
      </a>
    </div>
    <div class="section">
      <div class="main-title">
        New Time Request
        <span class="date">{{ $mailData['time'] }}</span>
      </div>
      <div class="section-row">
        <span class="title">Request from:</span>
        <span class="content-text">{{ $mailData['employee'] }}</span>
      </div>
      <div class="section-row">
        <span class="title">Department:</span>
        <span class="content-text">{{ $mailData['department'] }}</span>
      </div>
      <div class="section-row">
        <span class="title">Job Title:</span>
        <span class="content-text">{{ $mailData['position'] }}</span>
      </div>
    </div>
    <hr />
    <div class="section">
      <div class="section-row">
        <span class="title">Request Type:</span>
        <span class="content-text">{{ $mailData['request_type'] }}</span>
      </div>
      <div class="section-row">
        <span class="title">From:</span>
        <span class="content-text">{{ $mailData['from'] }}</span>
      </div>
      <div class="section-row">
        <span class="title">To:</span>
        <span class="content-text">{{ $mailData['to'] }}</span>
      </div>
      <div class="section-row">
        <span class="text">
          This will count as <span class="main-text">{{ $mailData['total_hours'] }} hour</span>
        </span>
      </div>
    </div>
    <div class="section">
      <p class="text">Please approve or deny the request:</p>
      <div class="button-group">
        <a href="{{ route('approve.request.email', ['id' => $request->id, 'admin_id' => $mailData['admin_id']]) }}?status=first_approved" class="button approve">Approve</a>
        <a href="{{ route('reject.request.email', ['id' => $request->id]) }}?status=rejected" class="button reject">Reject</a>
      </div>
    </div>
  </div>
</body>

</html>