<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Reset Password Email Template</title>
    <meta name="description" content="Reset Password Email Template">
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            font-family: Arial, sans-serif;
        }

        body {
            background-color: #f2f3f8;
        }

        .container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            border-radius: 4px;
            padding: 20px;
            box-shadow: 0 6px 18px rgba(0, 0, 0, 0.06);
        }

        .logo {
            text-align: center;
            margin-bottom: 20px;
        }

        .logo img {
            max-width: 150px;
        }

        h1 {
            color: #1e1e2d;
            font-size: 24px;
            font-weight: 500;
            margin-bottom: 10px;
        }

        hr {
            border: none;
            height: 1px;
            background-color: #cecece;
            margin: 20px 0;
        }

        p {
            color: #455056;
            font-size: 16px;
            line-height: 24px;
            margin-bottom: 10px;
        }

        .button {
            background-color: #0A588E;
            color: #fff !important;
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
            border-radius: 4px;
            padding: 10px 20px;
            display: inline-block;
            margin-bottom: 15px;
        }

        .footer {
            text-align: center;
            margin-top: 40px;
        }

        .footer p {
            font-size: 14px;
            color: rgba(69, 80, 86, 0.74);
            line-height: 18px;
            margin: 0;
        }

        .footer a {
            color: #0A588E;
            text-decoration: none;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="logo">
            <a href="{{ env('FRONT_URL') }}" target="_blank">
                <img src="https://scandinaviatech.com/_next/image?url=%2Fassets%2Flogo%2Flogo-portrait.png&w=1920&q=75" alt="Logo">
            </a>
        </div>
        <h1>Set Your Password</h1>
        <hr>
        <p>An account has been created for you. Please click the button below to set your password:</p>

        <a href="{{ env('FRONT_URL') }}/set-password?user_id={{ $mailData['user_id'] }}&token={{ $mailData['secure_token'] }}" class="button">Set Password</a>
        <p>Please note that the link will expire after 20 minutes.</p>
        <p>If you did not request this account or have any questions, please contact us immediately.</p>
        <div class="footer">
            <p>&copy; <a href="{{ env('FRONT_URL') }}">{{ env('FRONT_DOMAIN') }}</a></p>
        </div>
    </div>

</body>

</html>