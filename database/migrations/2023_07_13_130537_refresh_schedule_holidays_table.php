<?php

use App\Enums\Weekdays;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefreshScheduleHolidaysTable extends Migration
{

    public function up()
    {
        Schema::table('schedule_holidays', function (Blueprint $table) {
            $table->enum('day', Weekdays::getAllValues())->after('schedule_id');
        });
    }

    public function down()
    {
        Schema::table('schedule_holidays', function (Blueprint $table) {
            $table->dropColumn('day');
        });
    }
};
