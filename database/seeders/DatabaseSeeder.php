<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Factory;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            DepartmentSeeder::class,
            BranchSeeder::class,
            PermissionSeeder::class,
            // UserSeeder::class,
            BalanceSeeder::class,
            TypeSeeder::class,
            // UserBalanceSeeder::class,
            // ProjectSeeder::class,
            // UserProjectSeeder::class,
        ]);
    }
}
