<?php

namespace Database\Seeders;

use App\Enums\ProjectStatus;
use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    public function run()
    {
        Project::create([
            'name' => 'Lanura and Trama',
            'start_date' => '2022-05-01',
            'end_date' => '2022-06-01',
            'status' => ProjectStatus::in_progress->value,
            'department_id' => '1',
            'branch_id' => '2'
        ]);

        Project::create([
            'name' => 'Ouance UAE',
            'status' => ProjectStatus::done->value,
            'department_id' => '1',
            'branch_id' => '2'
        ]);

        Project::create([
            'name' => 'Nur Namee',
            'status' => ProjectStatus::done->value,
            'department_id' => '2',
            'branch_id' => '2'
        ]);

        Project::create([
            'name' => 'Sehnawi',
            'status' => ProjectStatus::pending->value,
            'department_id' => '1',
            'branch_id' => '1'
        ]);
    }
}
