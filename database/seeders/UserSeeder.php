<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        $user = User::create([
            'first_name' => 'Abdullah',
            'last_name' => 'Saqer',
            'email' => 'abdullah.saqer@test.com',
            'position' => 'CTO',
            'password' => bcrypt('00000000'),
            'is_student' => false,
            'personal_email' => 'tes2t@test.com',
            'address' => 'syria - damascus',
            'department_id' => 1,
            'branch_id' => 1,
            'image' => 'images/User-avatar.png',
            'phone' => '0991996099'
        ]);

        $user = User::create([
            'first_name' => 'Salah',
            'last_name' => 'Ebrahim',
            'email' => 'salah.ebrahim@test.com',
            'position' => 'Founder',
            'password' => bcrypt('00000000'),
            'is_student' => false,
            'personal_email' => 'test3@test.com',
            'address' => 'syria - damascus',
            'department_id' => 1,
            'branch_id' => 2,
            'image' => 'images/User-avatar.png',
            'phone' => '0991996999'
        ]);

        $user = User::create([
            'first_name' => 'Malek',
            'last_name' => 'Diab',
            'email' => 'malek.diab@test.com',
            'position' => 'Marketing Manger',
            'password' => bcrypt('00000000'),
            'is_student' => false,
            'personal_email' => 'test4@test.com',
            'address' => 'syria - damascus',
            'department_id' => 2,
            'branch_id' => 2,
            'image' => 'images/User-avatar.png',
            'phone' => '0991997899'
        ]);

        $user = User::create([
            'first_name' => 'Osama',
            'last_name' => 'El-Sawaf',
            'email' => 'gfosama.1999@gmail.com',
            'position' => 'React Developer',
            'password' => bcrypt('00000000'),
            'is_student' => true,
            'start_date' => '2022-01-11',
            'personal_email' => 'test6@test.com',
            'address' => 'syria - damascus',
            'department_id' => 1,
            'branch_id' => 1,
            'image' => 'images/User-avatar.png',
            'phone' => '0998997899'
        ]);

        $user = User::create([
            'first_name' => 'Youssef',
            'last_name' => 'Al-Farra',
            'email' => 'youssef.alfarra@gmial.com',
            'position' => 'content writer',
            'password' => bcrypt('00000000'),
            'is_student' => false,
            'start_date' => '2022-03-11',
            'personal_email' => 'test7@test.com',
            'address' => 'syria - damascus',
            'department_id' => 2,
            'branch_id' => 1,
            'image' => 'images/User-avatar.png',
            'phone' => '0998997299'
        ]);
    }
}
