<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\UserBalance;

class UserBalanceSeeder extends Seeder
{
    public function run()
    {
        $UserBalance = UserBalance::create(['user_id' => 5, 'balance_id' => 1, 'amount' => 27]);
        $UserBalance = UserBalance::create(['user_id' => 5, 'balance_id' => 2, 'amount' => 16]);

        $UserBalance = UserBalance::create(['user_id' => 6, 'balance_id' => 1, 'amount' => 16]);
    }
}
