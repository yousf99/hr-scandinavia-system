<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Branch;

class BranchSeeder extends Seeder
{
    public function run()
    {
        Branch::create(['name' => 'Syria']);
        Branch::create(['name' => 'Dubai']);
    }
}
