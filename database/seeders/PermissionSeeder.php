<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            //Project
            'project_access',
            'project_control',

            //Branches
            'branch_access',

            //Departments
            'department_access',

            //Type
            'type_access',
            'type_control',

            //Balance
            'balance_access',
            'balance_control',

            //Role
            'role_access',
            'role_control',

            //User
            'user_access',
            'user_control',

            //Request
            'request_access',
            'final_approve_request',
            'add_request_by_admin',

            //Statistic
            'view_statistics',
        ];
        $data = array_map(fn (String $value) => ['name' => $value, 'guard_name' => 'web'], $permissions);
        Permission::insert($data);
        $super_admin = Role::create(['name' => 'super_admin', 'guard_name' => 'web']);
        $permissions = Permission::all();
        $super_admin->syncPermissions($permissions);

        $non_super_admin_permissions = [
            //First Approve Development
            'first_approve_overtime_development',
            'first_approve_vacation_development',
            //First Approve Social Media
            'first_approve_overtime_social_media',
            'first_approve_vacation_social_media',
        ];
        $data2 = array_map(fn (String $value) => ['name' => $value, 'guard_name' => 'web'], $non_super_admin_permissions);
        Permission::insert($data2);

        $user = User::create([
            'first_name' => 'Abdullah',
            'last_name' => 'Saqer',
            'email' => 'abdullah.saqer@scandinaviatech.com',
            'position' => 'CTO',
            'password' => bcrypt('00000000'),
            'is_student' => false,
            'personal_email' => 'abdullah.saqer-@gmail.com',
            'address' => 'syria - damascus',
            'department_id' => 1,
            'branch_id' => 1,
            'image' => 'images/User-avatar.png',
            'phone' => '0966330991'
        ]);
        $user->assignRole('super_admin');
    }
}
