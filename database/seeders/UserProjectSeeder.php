<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\UserProjects;

class UserProjectSeeder extends Seeder
{
    public function run()
    {
        UserProjects::create(['user_id' => 2, 'project_id' => 1]);
        UserProjects::create(['user_id' => 3, 'project_id' => 1]);

        UserProjects::create(['user_id' => 5, 'project_id' => 1]);
        UserProjects::create(['user_id' => 5, 'project_id' => 2]);

        UserProjects::create(['user_id' => 6, 'project_id' => 3]);
    }
}
