<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    public function run()
    {
        Department::create(['name' => 'Development']);
        Department::create(['name' => 'Social Media']);
    }
}
