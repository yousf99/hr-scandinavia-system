<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    public function run()
    {
        Type::create(['name' => 'Paid Vacation', 'factor' => 1, 'balance_id' => 1]);
        Type::create(['name' => 'Exam Vacation', 'factor' => 1, 'balance_id' => 2]);
        Type::create(['name' => 'Sick Leave', 'factor' => 0,]);
        Type::create(['name' => 'Un Paid Vacation', 'factor' => 0,]);

        Type::create(['name' => 'Overtime at Home', 'factor' => 1.5,]);
        Type::create(['name' => 'Overtime at Company', 'factor' => 1,]);
        Type::create(['name' => 'Overtime at Company on Holiday', 'factor' => 2,]);
    }
}
