<?php

namespace Database\Seeders\live_version;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class FixSuperAdminRole extends Seeder
{
    public function run()
    {
        $permissions = Permission::all();
        $super_admin = Role::where('name', 'super_admin')->first();
        $super_admin->syncPermissions($permissions);
    }
}
