<?php

namespace Database\Seeders\live_version;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddPositionPermissionsSeeder extends Seeder
{
    public function run()
    {
        $position_access = Permission::create(['name' => 'position_access']);
        $position_control = Permission::create(['name' => 'position_control']);

        $superAdminRole = Role::where('name', 'super_admin')->first();

        $superAdminRole->givePermissionTo($position_access);
        $superAdminRole->givePermissionTo($position_control);
    }
}
