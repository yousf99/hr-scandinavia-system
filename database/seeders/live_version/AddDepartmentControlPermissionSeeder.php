<?php

namespace Database\Seeders\live_version;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddDepartmentControlPermissionSeeder extends Seeder
{
    public function run()
    {
        // Create the new permission
        $permission = Permission::create(['name' => 'department_control']);

        // Find the super_admin role
        $superAdminRole = Role::where('name', 'super_admin')->first();

        // Assign the permission to the super_admin role
        $superAdminRole->givePermissionTo($permission);
    }
}
