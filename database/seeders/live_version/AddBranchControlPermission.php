<?php

namespace Database\Seeders\live_version;

// use App\Models\Permission;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddBranchControlPermission extends Seeder
{
    public function run()
    {
        $permission = Permission::create(['name' => 'branch_control']);
        $superAdminRole = Role::where('name', 'super_admin')->first();
        $superAdminRole->givePermissionTo($permission);
    }
}
