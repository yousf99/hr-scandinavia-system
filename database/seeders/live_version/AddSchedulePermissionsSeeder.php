<?php

namespace Database\Seeders\live_version;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddSchedulePermissionsSeeder extends Seeder
{
    public function run()
    {
        // Create the new permissions
        $permissions = [
            'schedule_access',
            'schedule_control',
        ];

        foreach ($permissions as $permissionName) {
            Permission::firstOrCreate(['name' => $permissionName, 'guard_name' => 'web']);
        }

        // Find the super_admin role
        $superAdminRole = Role::where('name', 'super_admin')->first();

        // Assign the permissions to the super_admin role
        $superAdminRole->syncPermissions($permissions);
    }
}
