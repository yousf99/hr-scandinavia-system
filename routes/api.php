<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1;
use App\Http\Controllers;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Controllers\FixController;

//Auth
Route::controller(v1\AuthController::class)->prefix('auth')->group(function () {
    Route::post('login', 'login');
    Route::post('set_password', 'setPassword');
    Route::post('forget_password', 'forgetPassword');
    Route::post('change_password', 'changePassword');
    Route::post('device_token', 'deviceToken');
});

//Auth With login Permission
Route::controller(v1\AuthController::class)->prefix('auth')->middleware(['auth:sanctum'])->group(function () {
    Route::get('me', 'me');
    Route::post('logout', 'logout');
    Route::put('update_profile', 'updateProfile');
    Route::post('change_password', 'changePassword');
});



Route::middleware(['auth:sanctum'])->group(function () {

    //Users
    Route::apiResource('users', v1\UserController::class)->only('index', 'update', 'store');
    Route::controller(v1\UserController::class)->prefix('users')->group(function () {
        Route::get('show/{user}', 'show');
        Route::delete('soft_delete/{user}', 'softDelete');
        Route::post('restore/{id}', 'restore');
        Route::put('make_not_student/{user}', 'makeNotStudent');
        Route::put('make_student/{user}', 'makeStudent');
    });
    Route::post('users/upload_image', [v1\ApiController::class, 'upload_image']);

    //Requests
    Route::apiResource('requests', v1\RequestController::class)->only('index', 'store');
    Route::controller(v1\RequestController::class)->prefix('requests')->group(function () {
        Route::get('show/{request}', 'show');
        Route::post('approve/{request}', 'approveRequest');
        Route::post('reject/{request}', 'rejectRequest');
        Route::post('add_request_to_user', 'addRequestByAdmin');
        Route::delete('delete/{id}', 'destroy');
        Route::get('my_requests', 'myRequests');
        Route::get('absent_today', 'absentToday');
        Route::post('calculate_hours_overtime/{id}', 'calculateOvertimeHours');
    });

    //Types
    Route::apiResource('types', v1\TypeController::class)->only('index', 'update', 'store');
    Route::controller(v1\TypeController::class)->prefix('types')->group(function () {
        Route::get('show/{type}', 'show');
        Route::delete('delete/{type}', 'softDelete');
        Route::get('my_types', 'myTypes');
    });

    //Departments
    Route::apiResource('departments', v1\DepartmentController::class)->only('index', 'store', 'update');
    Route::controller(v1\DepartmentController::class)->prefix('departments')->group(function () {
        Route::delete('delete/{department}', 'destroy');
    });

    //Projects
    Route::apiResource('projects', v1\ProjectController::class)->only('index', 'store', 'update');
    Route::controller(v1\ProjectController::class)->prefix('projects')->group(function () {
        Route::get('show/{project}', 'show');
        Route::get('my_projects', 'myProjects');
        Route::post('add_user/{project}', 'addUserToProject');
        Route::post('remove_user/{project}', 'removeUserFromProject');
        Route::delete('delete/{project}', 'destroy');
    });

    // Balance
    Route::apiResource('balances', v1\BalanceController::class)->only('index', 'update', 'store');
    Route::controller(v1\BalanceController::class)->prefix('balances')->group(function () {
        Route::get('show/{balance}', 'show');
        Route::post('add_balance_to_user', 'addBalanceToUser');
        Route::post('remove_balance_from_user', 'removeBalanceFromUser');
        Route::put('update_user_balance/{user_id}', 'updateUserBalance');
    });

    //Statistics
    Route::controller(v1\StatisticsController::class)->prefix('statistics')->group(function () {
        Route::get('attendance', 'attendanceStatistics');
        Route::get('projects', 'projectsStatistics');
        Route::get('user_balances/{id}', 'userBalanceStatistics');
        Route::get('my_balances', 'myBalanceStatistics');
        Route::get('user_overtime_statistics/{id}', 'userOvertimeStatistics');
        Route::get('my_overtime_statistics', 'myOvertimeStatistics');
    });

    //Roles
    Route::controller(v1\RoleController::class)->prefix('roles')->group(function () {
        Route::get('all', 'index');
        Route::get('show/{role}', 'show');
        Route::put('update/{role}', 'update');
        Route::post('add', 'store');
        Route::delete('delete/{role}', 'destroy');
        Route::post('grant', 'grant');
        Route::post('revoke', 'revoke');
    });

    //Permissions
    Route::apiResource('permissions', v1\PermissionController::class)->only('index', 'update');
    Route::controller(v1\PermissionController::class)->prefix('permissions')->group(function () {
        Route::get('show/{permission}', 'show');
        Route::post('grant/{role}', 'grant');
        Route::post('revoke/{role}', 'revoke');
    });

    //Attachments
    Route::controller(Controllers\AttachmentController::class)->prefix('attachments')->group(function () {
        Route::post('upload_file', 'store');
        Route::put('update_file/{attachment}', 'update');
    });

    //Schedule
    Route::apiResource('schedules', v1\ScheduleController::class)->only('index', 'store');
    Route::controller(v1\ScheduleController::class)->prefix('schedules')->group(function () {
        Route::get('show/{schedule}', 'show');
    });

    //Notification
    Route::apiResource('notifications', v1\NotificationController::class)->only('index');
    Route::controller(v1\NotificationController::class)->prefix('notifications')->group(function () {
        Route::get('my_notifications', 'myNotifications');
    });

    //Position
    Route::apiResource('positions', v1\PositionController::class)->only('index', 'store', 'update');
    Route::controller(v1\PositionController::class)->prefix('positions')->group(function () {
        Route::get('show/{position}', 'show');
    });

    // branch
    Route::apiResource('branches', v1\BranchController::class)->only('index', 'store', 'update');
});

//Fix Api
Route::controller(FixController::class)->prefix('fix')->group(function () {
    Route::put('default_schedule', 'setDefaultScheduleForAllUsers');
    Route::get('get_all_role', 'getAllRole');
    Route::get('send_mail/{id}', 'sendMail');
    Route::get('test_time_zone', 'testTimeZone');
    Route::post('calculate_hours', 'calculateHours');
    Route::post('calculate_overtime_hours', 'calculateOvertimeHours');
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/send-notification', [Controller::class, 'sendNotificationToDevice']);
