<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ResponsePageController;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/approve/{id}/{admin_id}', [v1\RequestController::class, 'approveRequestFromEmail'])->name('approve.request.email');
Route::get('/reject/{id}', [v1\RequestController::class, 'rejectRequestFromEmail'])->name('reject.request.email');
Route::get('/approval-page', [ResponsePageController::class, 'approvalPage'])->name('approval-page');
Route::get('/reject-page', [ResponsePageController::class, 'rejectPage'])->name('reject-page');
