<?php

return [
    'firebase' => [
        'credentials' => [
            'type' => 'service_account',
            'project_id' => 'hrms-scandinavia',
            'private_key_id' => '44ee4f35a7db7db8bca8ce4ff358df5f24a7ad5f',
            'private_key' => '-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCguz3Rhh6sgigN
stbybQIp5P8XoEF2MHbv2sADo9Spr6ivFHLH7mjk4rJOpX+Zq7oieTIMZCGBeM0T
988zEBXUo07YuPxIZ2CZgcJs86fQ+mQaNckx180lc+6rNChBiZGtfsgxAryOXnsp
tegFPPb3lEafkqll0/sy5RRVkukvW20bNmV2cNV3isJoA5OixVBG0CNWm1Am5+TY
g7y///QF9jvXz0TW5x5sT8wFqrGfsy04C6jRzuz+pLhrdnUUNXoQRRkIk5axmm2C
P5ju1tx9dAgMbi5iqXcRAnAA9seIziljOpMstfHk/IbDa6M9w0z28hn4YKgmlaec
rK+BgTE7AgMBAAECggEAF1Z9/YrFBb+o/i2IrU7rq1aZ6Ch3k31/qjMFLLd156Te
+tD0G5iVOCARy36XSPeuLAGUpUedX9XA/T8UTEdFIYi20vFahvl6Wr+vEdp3yaME
Xuk5YJiDgTEp/JzdotUinGJAwhuyHNVAz90e+90OxQMNasUaU0k/e56B4IJXg8MT
m2G011tNXq08ZIRsz7qsPr1OXxZxanPj31tczk9HxD+Bw2M3JgBNvfdNmTlGs9Gu
aK4W/6/WWfQsqsIxZaSMpPR1ILhmFS1oGNdA+dwinwZz/XyTLuKBQBgq7dDvyU9w
L0l5Oj92x3BVQZ/oEX2bLMM716L9C5RiPvYAYEotoQKBgQDg6UJ6Mrka8/auX75J
+anqBXA1w2HNYviIlOvKxV5jyAPAMgLgNG3j1iaykQh+jd6Ffbj8haKOl89evglh
CfCNjdK1gw7kJLkJsu1yZe8L/26Qs5v1FfpnZpraHrZhUbtwQndInPzggr4pv716
PS3aG6gkV0GUS2RcfNN0vP3UWwKBgQC28uhCfWNsY7yHYQpbnSY3Bp5DP8KTGj0y
2G4RKvokeDzVExKUsdc19gbIlNW1+rKYTY5Y1X9JKvKagoZZWZV1NKfJve2BjL8H
KOtXy3R09ggTSNybrb9iBmsAYL3tn1PuN/LWK65C5oZajvRu8CxLsKaeMfXQKulA
z7wxUk0soQKBgEHHFqEkeoyOc0PWTpHWCcKeqo2ewdjOrD9NxpQ8Gvn1wlsQhVTL
IKR0ukaZMjHJDh5egZ6g+sTXU1ezHo0Z2hecNdbOKKnCr7dNdDJz63DH2K9LSD5q
mhxpnrpSJybZP4d6eOZsu2xx8GnHYQQCJonWctuGBXD4h7ag7zsCFv5nAoGAFOAG
rH+ONrHysyfZfLajw6eXq2i9FPfrf7++X2sd+8dUeaOhfSIFwnx+Laj98bPXaRWA
ZpaF3Z1u7T5zggJ80VDezGBFl4Ct021R3USz1xyzsy+fLIxYyCCfWP2w7ko903LC
3LlDOxL+xZqGPZpeE4zmlipxN/2Ne/24JZCUicECgYAZQ7xDah3m68thAbXDf7IQ
X8MdQmkffporB33e83LcXdJifbHKVw+cU7P16dfyJrqAoQIYSGfrOgSdAfiGJpWR
CGX2s0X0Aj5Zx6d20msbnFYQ5K/t1Oi6Y1LUg51Xmx9lVvxuVDgchqk07xt3n7Ld
brNuxvOpA8erF/3i+6dTsw==\n-----END PRIVATE KEY-----',
            'client_email' => 'firebase-adminsdk-q2mee@hrms-scandinavia.iam.gserviceaccount.com',
            'client_id' => '111623462788221409224',
            'auth_uri' => 'https://accounts.google.com/o/oauth2/auth',
            'token_uri' => 'https://oauth2.googleapis.com/token',
            'auth_provider_x509_cert_url' => 'https://www.googleapis.com/oauth2/v1/certs',
            'client_x509_cert_url' => 'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-q2mee%40hrms-scandinavia.iam.gserviceaccount.com',
            'universe_domain' => 'googleapis.com',
        ],
    ],
];
